import React from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, Image,} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Avatar, Caption, Title } from 'react-native-paper'
import COLOR from '../../utils/Tools';
import { BASE_URL } from '../../../store/constant/general';

const ProductDetailCard = (props) => {

  const PriceForm = (num) => {
    let str = num.toString(),
    split = str.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr( 0, sisa ),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <>
      <ScrollView style={styles.container}>
          <Image 
            source={{uri: `${BASE_URL}${props.image}`}}
            
            style={styles.imageProduct}
          />
        <View style={styles.titleProduct}>
          <Text style={styles.title}>{props.name}</Text>
          <Text style={styles.price}>Rp {""}
            { props.actualPrice
            ?PriceForm(props.actualPrice)
            :PriceForm(props.price.$numberDecimal)}
            ,-/kg
          </Text>
          <View style={{flexDirection: 'row'}}>
            {/* <Image 
              source={require('../../assets/Images/bintang.png')}
              style={{width: 80, height: 30, marginLeft: 15, marginTop: 5}}
            />
            <Text style={{fontSize: 12,marginLeft: 10, marginTop: 13}}>5.0</Text> */}
          </View>
          {/* <View style={styles.avatarSeller}>
            <Avatar.Image 
              source={{
                uri:'https://cdns.klimg.com/kapanlagi.com/p/headline/476x238/video-lama-kim-seon-ho-saat-berusia-20--88d000.jpg'}} 
                size={40}
              style={{marginTop: 10}}
            />
            <Text style={styles.nameSeller}>Kim Seon-Ho</Text>
            <TouchableOpacity style={styles.buttonSellerDetails}
              onPress={() => navigation.navigate('Profile')}
            >
              <Text style={{color: COLOR.darkGreen}}>Seller Details</Text>
            </TouchableOpacity>
          </View> */}
        </View>
      </ScrollView>
    </>
  )
}

export default ProductDetailCard

const styles = StyleSheet.create({
  container: {
    marginTop: 10, 
    backgroundColor: COLOR.yellow
  },
  imageProduct: {
    width: 400, 
    height: 350
  },
  titleProduct: {
    backgroundColor: COLOR.pureWhite,
    height: 100
  },
  title: {
    fontSize: 24, 
    paddingLeft: 15, 
    marginTop: 10
  },
  price:{
    fontSize: 18,
    color: COLOR.red, 
    paddingLeft: 15,
    marginTop: 10
  },
  avatarSeller: { 
    backgroundColor: COLOR.pureWhite, 
    marginTop: 10, 
    height: 60, 
    paddingLeft: 15, 
    flexDirection: 'row'
  },
  nameSeller: {
    marginTop: 17, 
    fontSize: 15, 
    fontWeight: '700', 
    marginLeft: 10
  },
  buttonSellerDetails: {
    borderWidth: 1, 
    borderColor: COLOR.green, 
    width: 110, 
    height: 30, 
    justifyContent: 'center', 
    alignItems: 'center', 
    borderRadius: 5, 
    marginLeft: 120, 
    marginTop: 15
  },
  description: {
    backgroundColor: COLOR.pureWhite, 
    marginTop: 10,
    height: 300
  }
})

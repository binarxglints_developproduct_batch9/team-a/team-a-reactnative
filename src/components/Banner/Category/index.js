import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity, Image } from 'react-native'

import COLOR from '../../../utils/Tools';

const Category = () => {
  return (
      <View style={styles.category}>
        <Text style={styles.textCategory}>Kategori</Text>
      <View style={{flexDirection: 'row'}}>
      <View style={styles.boxCategory}>
      <TouchableOpacity>
        <Image 
          source={require('../../../assets/Images/vegetable.png')} 
          style={styles.iconCategory}
        />
        <Text style={styles.textIcon}>All</Text>
      </TouchableOpacity>
      </View>
      <View style={styles.boxCategory}>
      <TouchableOpacity>
        <Image 
          source={require('../../../assets/Images/vegetables.png')} 
          style={styles.iconCategory}
        />
        <Text style={{fontSize: 12, color: COLOR.darkGreen, fontWeight: '700', marginLeft: 5}}>Vegetables</Text>
      </TouchableOpacity>
      </View>
      <View style={styles.boxCategory}>
      <TouchableOpacity>
        <Image 
          source={require('../../../assets/Images//fruits.png')} 
          style={styles.iconCategory}
        />
        <Text style={{fontSize: 12, color: COLOR.darkGreen, fontWeight: '700', marginLeft: 20}}>Fruits</Text>
      </TouchableOpacity>
      </View>
      <View style={styles.boxCategory}>
      <TouchableOpacity>
        <Image 
          source={require('../../../assets/Images/diet.png')} 
          style={styles.iconCategory}
        />
        <Text style={{fontSize: 12, color: COLOR.darkGreen, fontWeight: '700', marginLeft: 20}}>Diets</Text>
      </TouchableOpacity>
      </View>
      </View>
      </View>
  )
}

export default Category

const styles = StyleSheet.create({
  category: {
    backgroundColor:COLOR.green, 
    marginTop: 15, 
    width: 383,
    height: 120,
    borderRadius: 10,
    marginLeft: 5,
  },
  textCategory: {
    fontSize: 18, 
    color:COLOR.pureWhite, 
    fontWeight:'bold', 
    textDecorationLine:'underline', 
    alignSelf: 'center', 
    marginBottom: 10
  },
  boxCategory: {
    backgroundColor:COLOR.pureWhite, 
    width: 70,
    borderRadius: 10,
    marginLeft: 20,
  },
  iconCategory: {
    height: 50, 
    width:50,
    alignSelf: 'center'
  },
  textIcon: {
    color: COLOR.darkGreen,
    fontSize: 16,
    fontWeight: '700',
    marginLeft: 25
  }
})

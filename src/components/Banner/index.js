import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

import Swiper from 'react-native-swiper';

import COLOR from '../../utils/Tools';

const Banner = () => {
  return (
    <View style={styles.sliderContainer}>
    <Swiper autoplay horizontal={false} height={200} activeDotColor={COLOR.green} >
      <View style={styles.slides} >
        <Image 
          source={require('../../assets/Images/banner1.jpg')}
          resizeMode="cover"
          style={styles.sliderImage}
        />
      </View>
      <View style={styles.slides} >
        <Image 
          source={require('../../assets/Images/banner2.jpg')}
          resizeMode="cover"
          style={styles.sliderImage}
        />
      </View>
      <View style={styles.slides} >
        <Image 
          source={require('../../assets/Images/banner3.jpg')}
          resizeMode="cover"
          style={styles.sliderImage}
        />
      </View>
      <View style={styles.slides} >
        <Image 
          source={require('../../assets/Images/banner4.jpg')}
          resizeMode="cover"
          style={styles.sliderImage}
        />
      </View>
      <View style={styles.slides} >
        <Image 
          source={require('../../assets/Images/banner5.jpg')}
          resizeMode="cover"
          style={styles.sliderImage}
        />
      </View>
    </Swiper>
    </View>
  )
}

export default Banner

const styles = StyleSheet.create({
  sliderContainer: {
    height: 200,
    width: '97%',
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 10,
  },

  wrapper: {},

  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderRadius: 8,
  },
  sliderImage: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 8,
  },
  categoryContainer: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 25,
    marginBottom: 10,
  },
})

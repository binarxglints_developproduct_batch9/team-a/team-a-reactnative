import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

import Feather from 'react-native-vector-icons/Feather';

import COLOR from '../../../utils/Tools';

const ContentProfile = () => {
	return (
		<View>
			<View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>History Transactions</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>My Address</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>Forgot Password</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>Help</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>Contact Us</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>Terms & Conditions</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View style={{backgroundColor: COLOR.white, marginTop: 10, height: 30}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{marginTop: 5, paddingLeft: 15}}>Privacy Policy</Text>
          <TouchableOpacity onPress={()=> {}}>
            <Feather name="arrow-right-circle" size={23} style={styles.Icon}/> 
          </TouchableOpacity>
        </View>
      </View>
      <View>
		</View>
	</View>
	)
}

export default ContentProfile

const styles = StyleSheet.create({
	Icon: {
    color: COLOR.green,
    paddingRight: 15,
    marginTop: 3
  }
})

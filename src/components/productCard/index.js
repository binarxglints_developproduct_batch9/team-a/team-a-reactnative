import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import COLOR from '../../utils/Tools'
import { BASE_URL } from '../../../store/constant/general';
import {getDetailProductsAction} from '../../../store/action/detailProductAction';
import {useSelector, useDispatch} from 'react-redux';

const ProductCard = (props) => {

  const dispatch = useDispatch();

  const PriceForm = (num) => {
    let str = num.toString(),
    split = str.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr( 0, sisa ),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => {
        dispatch(getDetailProductsAction(props.id))
        props.detailProduct()}}>
        <Image 
          source={{uri: `${BASE_URL}${props.image}`}}
          style={{height: 200, width: 200}}
        />
        <Text style={styles.title}>{props.name}</Text>
        <Text style={styles.subtitle}>Rp {""}
        { props.actualPrice
        ?PriceForm(props.actualPrice)
        :PriceForm(props.price.$numberDecimal)}
        ,-/kg
        </Text>
        
      </TouchableOpacity>
    </View>
  )
}

export default ProductCard

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.pureWhite, 
    marginTop: 15, 
    marginLeft: 10
  },
  title: {
    fontWeight: '700', 
    fontSize: 18,
    paddingLeft: 10
  },
  subtitle: {
    color: COLOR.red, 
    fontSize: 16,
    paddingLeft: 10
  }
})

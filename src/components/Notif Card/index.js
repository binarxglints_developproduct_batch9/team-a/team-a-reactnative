import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import COLOR from '../../utils/Tools';
import { useDispatch, useSelector } from 'react-redux';
import { BASE_URL } from '../../../store/constant/general';

const NotifCard = (props) => {
  console.log("Notif Card", props)

  const dispatch = useDispatch();

  const PriceForm = (num) => {
    let str = num.toString(),
    split = str.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr( 0, sisa ),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <View style={styles.containerCard}>
      <View style={styles.content}>
        <Image 
          source={{uri: `${BASE_URL}${props.image}`}}
          style={{width: 150, height: 150}}
        />
        <View style={{paddingLeft: 15, marginTop: 10}}>
          <Text style={{fontSize: 18, fontWeight: '700'}}>{props.name}</Text>
          <Text style={{fontSize: 14}}>Rp {""}
        { props.actualPrice
        ?PriceForm(props.actualPrice)
        :PriceForm(props.price)}
        ,-/kg
        </Text>
          {props.status == 'success'? (
            <>
            <Text style={{fontSize: 16, marginTop: 15, color: COLOR.darkGreen}}>{props.buyer} has paid for this product</Text>
          <TouchableOpacity style={styles.box}>
            <Text style={{color: COLOR.pureWhite, fontWeight: '700', fontSize: 14}}>Confirm</Text>
          </TouchableOpacity>
            </>
          )
          :
          props.status == 'on process'? (
            <Text style={{fontSize: 16, marginTop: 15, color: COLOR.orange}}>Waiting for payment</Text>
          )
          :
          (<Text style={{fontSize: 16, marginTop: 15, color: COLOR.red}}>{props.buyer} has been cancelled</Text>)
          }
          
        </View>
        
      </View>
    </View>
  )
}

export default NotifCard

const styles = StyleSheet.create({
  containerCard: {
    backgroundColor: COLOR.pureWhite, 
    marginTop: 15, 
    width: 350, 
    height: 150, 
    borderRadius: 10,
    alignSelf: 'center'
  },
  content: {
    backgroundColor: COLOR.yellow, 
    borderRadius: 10, 
    width: 150, 
    flexDirection: 'row'
  },
  box: {
    borderWidth: 1, 
    borderColor: COLOR.darkGreen,
    backgroundColor: COLOR.green,
    borderRadius: 3, 
    marginTop: 5, 
    alignItems: 'center',
    justifyContent: 'center'
  }
})
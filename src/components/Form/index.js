import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, ScrollView, Image, Picker} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import COLOR from '../../utils/Tools'
import FormData from 'form-data';
import axios from 'axios';
import {BASE_URL, TOKEN} from '../../../store/constant/general';
import * as ImagePicker from 'react-native-image-picker'
import Modal from 'react-native-modal';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Form = () => {

  // const [selectedValue, setSelectedValue] = useState("category");

  const [ productName, setProductName ] = useState('');
  const [ seller, setSeller ] = useState('');
  const [ category, setCategory ] = useState('category');
  const [ price, setPrice ] = useState('');
  const [ stock, setStock ] = useState('');
  const [ description, setDescription ] = useState('');
  const [image, setImage] = useState({type: '', uri: '', name: ''})

  const createProduct= async () => {
      let data = new FormData();
      data.append('name', productName);
      data.append('price', price);
      data.append('desc', description);
      data.append('stock', stock);
      data.append('category', category);
      data.append('image', image);
      const response = await axios.post (`${BASE_URL}/home/create`, data, {'headers': { 'Authorization': `Bearer ${TOKEN}`}})
      console.log('Pagi Dunia', response);
      setProductName('');
      setSeller('')
      setCategory('')
      setPrice('')
      setStock('')
      setDescription('')
      setImage({type: '', uri: '', name: ''})
  }

  const options = {
    title: 'Pick an image',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const openPicker = () => {
    ImagePicker.launchImageLibrary (options, (response) => {
      console.log('response=', response)
      if (response.error) {
        console.log('LaunchImageLibrary Error:', response.error);
      } else {
        const source = {uri: response.uri};
        setImage({ type: response.type, uri: response.uri, name: response.fileName })
        console.log(source)
      }
    });
    
  };

  const navigation = useNavigation()
  return (
    <ScrollView>
      <View>
        <View style={{marginTop: 10, borderRadius: 10,backgroundColor: COLOR.grey, alignSelf: 'center'}}>
          <TouchableOpacity onPress={openPicker}>
            <Image 
              source={{uri: image.uri}}
              style={{width: 300, height: 200}}
            />
          </TouchableOpacity>
            {/* <View style={{justifyContent: 'center', alignItems: 'center', marginTop: -100}}>
            <TouchableOpacity onPress={openPicker}>
              <Icon 
                name='add-box' 
                size={35}
                color={COLOR.white}
              />
            </TouchableOpacity>
            </View> */}
          </View>
        </View>
      <View style={{paddingLeft: 15, marginTop: 25}}>
        <Text style={{marginTop: 5}}>Product Name</Text>
        <TextInput 
          placeholder=''
          value={productName}
          onChangeText={(text)=> setProductName(text)}
          style={styles.box}
        />
        {/* <Text style={{marginTop: 5}}>Seller</Text>
        <TextInput 
          placeholder=''
          value={seller}
          onChangeText={(text)=> setSeller(text)}
          style={styles.box}
        /> */}
        {/* <Text style={{marginTop: 5}}>Category</Text> */}
        {/* <TextInput 
          placeholder=''
          value={category}
          onChangeText={(text)=> setCategory(text)}
          style={styles.box}
        /> */}
        <Picker
        selectedValue={category}
        // style={{ height: 50}}
        onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}
        // style={styles.box}
      >
        <Picker.Item label="category" value="category" />
        <Picker.Item label="fruit" value="fruit" />
        <Picker.Item label="vegetable" value="vegetable" />
      </Picker>
        <Text style={{marginTop: 5}}>Price</Text>
        <TextInput 
          placeholder=''
          value={price}
          onChangeText={(text)=> setPrice(text)}
          style={styles.box}
        />
        <Text style={{marginTop: 5}}>Stock</Text>
        <TextInput 
          placeholder=''
          value={stock}
          onChangeText={(text)=> setStock(text)}
          style={styles.box}
        />
        <Text style={{marginTop: 5}}>Description</Text>
        <TextInput 
          placeholder=''
          value={description}
          onChangeText={(text)=> setDescription(text)}
          style={styles.desc}
        />
        <TouchableOpacity
          style={{width: 370, height: 50, borderRadius: 5, backgroundColor: COLOR.green, justifyContent: 'center', alignItems: 'center', marginTop: 20}}
          onPress={() => {
            createProduct();
            navigation.navigate('Home')
          }}
        >
          <Text style={{fontWeight: '700', fontSize: 18, color: COLOR.pureWhite}}> Save </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  )
}

export default Form

const styles = StyleSheet.create({
  box: {
    borderWidth: 1, 
    borderColor: COLOR.green, 
    width: 370, 
    height: 50, 
    borderRadius: 5, 
    backgroundColor: COLOR.white, 
    marginTop: 5,
    paddingLeft: 10
  },
  container: {
    flex: 1,
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: COLOR.green,
    alignItems: 'center',
    marginTop: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: COLOR.white,
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: COLOR.white,
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: COLOR.black,
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: COLOR.green,
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLOR.green,
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  desc: {
    borderWidth: 1, 
    borderColor: COLOR.green, 
    width: 370, 
    height: 110, 
    borderRadius: 5, 
    backgroundColor: COLOR.white, 
    marginTop: 5,
    paddingLeft: 10
  }
})

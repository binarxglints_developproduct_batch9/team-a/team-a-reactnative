import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, Button } from 'react-native'
import COLOR from '../../utils/Tools'
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { BASE_URL } from '../../../store/constant/general';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { deleteMyProductsAction } from '../../../store/action/deleteMyProductsAction';

const MyProducts = (props) => {
  console.log('My Products Card:', props)

  const data = useSelector(state => state.deleteMyProductsReducer.data)
  const dispatch = useDispatch();

  const navigation = useNavigation()

  const handleDelete = (id)=>{
    dispatch(deleteMyProductsAction(id))
    console.log('products Id', id)
  }

  const PriceForm = (num) => {
    let str = num.toString(),
    split = str.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr( 0, sisa ),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <View style={styles.container}>
      {/* <TouchableOpacity onPress={() => {
        dispatch(getDetailProductsAction(props.id))
        props.detailProduct()}}> */}
        <TouchableOpacity>
        <Image 
          source={{uri: `${BASE_URL}${props.image}`}}
          style={{height: 200, width: 180}}
        />
        <Text style={styles.title}>{props.name}</Text>
        <Text style={styles.subtitle}>Rp {""}
        { props.actualPrice
        ?PriceForm(props.actualPrice)
        :PriceForm(props.price)}
        ,-/kg</Text>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', height: 45}}>
        <View style={{borderWidth: 1, width: 90, justifyContent: 'center', alignItems: 'center', backgroundColor: COLOR.green, borderBottomLeftRadius: 10, marginTop: 5}}>
          <TouchableOpacity onPress={() => navigation.navigate('Edit Product')}>
              <FontAwesome 
                name="edit"
                size={20}
              />
          </TouchableOpacity>
        </View>
        <View style={{borderWidth: 1, width: 90, justifyContent: 'center', alignItems: 'center', backgroundColor: COLOR.red, borderBottomRightRadius: 10, marginTop: 5}}>
          <TouchableOpacity onPress={() => handleDelete()}>
            <FontAwesome 
                name="trash-o"
                size={20}
            />
          </TouchableOpacity>
        </View>
      </View>
      
    </View>
  )
}

export default MyProducts

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.pureWhite, 
    marginTop: 15, 
    marginLeft: 10,
    width: 180,
  },
  title: {
    fontWeight: '700', 
    fontSize: 18,
    paddingLeft: 10
  },
  subtitle: {
    color: COLOR.red, 
    fontSize: 16,
    paddingLeft: 10
  }
})
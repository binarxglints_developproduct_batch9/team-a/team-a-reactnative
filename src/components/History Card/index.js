import React from 'react'
import { StyleSheet, Text, View, ScrollView, StatusBar, Image } from 'react-native'
import { useDispatch } from 'react-redux';
import COLOR from '../../utils/Tools/index';
import { BASE_URL } from '../../../store/constant/general';

const HistoryCard = (props) => {
  console.log('ara nyerah edit', props)
  const dispatch = useDispatch();

  const PriceForm = (num) => {
    let str = num.toString(),
    split = str.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr( 0, sisa ),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <View style={{backgroundColor: COLOR.pureWhite, marginTop: 10, width: 370, height: 200, alignSelf: 'center', borderRadius: 10}}>
       <View style={{flexDirection: 'row', paddingLeft: 15, paddingTop: 10}}>
       <Image source={{uri: `${BASE_URL}${props.sellerImage}`}} 
         style={{width: 30, height: 30, borderRadius: 100}}
       />
          <Text style={{marginLeft: 10, fontSize: 14}}>{props.sellerUsername}</Text>
          </View>
       
       <View style={{flexDirection: 'row', marginLeft: 15, marginTop: 15}}>
          <Image source={{uri: `${BASE_URL}${props.image}`}}
            style={{width: 170, height: 130}}
          />
          <View style={{marginTop: 15, marginLeft: 15}}>
            <Text style={{fontSize: 18, fontWeight: 'bold' }}>{props.name}</Text>
            <Text style={{fontSize: 16, color: COLOR.red, marginTop: 15}}>Rp {""}
              { props.actualPrice
              ?PriceForm(props.actualPrice)
              :PriceForm(props.price)}
              ,-/kg</Text>
            {props.status == 'success' ? 
            (<Text style={{fontSize: 18, fontWeight: 'bold', marginTop: 20, color: COLOR.green}}>{props.status}</Text>) 
            :
            props.status == 'on process' ?
            (<Text style={{fontSize: 18, fontWeight: 'bold', marginTop: 20, color: COLOR.yellow}}>{props.status}</Text>)
            :
            (<Text style={{fontSize: 18, fontWeight: 'bold', marginTop: 20, color: COLOR.red}}>{props.status}</Text>)
            }
          </View>
        </View>
      
    </View>
  )
}

export default HistoryCard

const styles = StyleSheet.create({})
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native'

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator, HeaderHeightContext } from '@react-navigation/stack';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from "react-native-vector-icons/AntDesign";

import COLOR from '../utils/Tools';

import { 
  HistoryScreen,
  HomeScreen, MyProductsScreen,
  ProfileScreen
} from '../screen'

const HomeStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabs = () => {
  return (
    <Tab.Navigator 
      initialRouteName='Home'
      // tabBarColor={COLOR.orange}
      barStyle= {{
        backgroundColor: COLOR.white
      }}
      // TabBarOptions={{
      //   activeTintColor: COLOR.green,
      //   inactiveTintColor: COLOR.grey
      // }}
      activeTintColor={COLOR.green}
      inactiveTintColor={COLOR.grey}
    >
      <Tab.Screen 
        name="Home" 
        // component={HomeStackScreen}
        component={HomeScreen}
        options={{
          tabBarLabel:"Home",
          tabBarColor:(COLOR.white),
          tabBarIcon:({color}) => (
            <FontAwesome 
              name='home'
              color={COLOR.green}
              size={26}
            />
          ),
        }}
      />
      <Tab.Screen 
        name="History" 
        // component={HomeStackScreen}
        component={HistoryScreen}
        options={{
          tabBarLabel:"History",
          tabBarColor:(COLOR.white),
          tabBarIcon:({color}) => (
            <FontAwesome 
              name='handshake-o'
              color={COLOR.green}
              size={26}
            />
          ),
        }}
      />
      <Tab.Screen 
        name="My Product" 
        component={MyProductsScreen}
        options={{
          tabBarLabel:"My Products",
          tabBarColor:(COLOR.white),
          tabBarIcon:({color}) => (
            <AntDesign 
              name='isv'
              color={COLOR.green}
              size={24}
            />
          ),
        }}
      />
      <Tab.Screen 
        name="Profile" 
        component={ProfileScreen}
        options={{
          tabBarLabel:"Account",
          tabBarColor:(COLOR.white),
          tabBarIcon:({color}) => (
            <FontAwesome 
              name='user'
              color={COLOR.green}
              size={24}
            />
          ),
        }}
      />
    </Tab.Navigator>
  )
}
export default MainTabs;

// const HomeStackScreen= () => {
//   return(
//     <HomeStack.Navigator>
//       <HomeStack.Screen 
//         name="Home" 
//         component={HomeScreen} 
//         options={{
//           headerTitle: '',
//           headerShown: false,
//         }}
//       />
//       <HomeStack.Screen 
//         name="Search" 
//         component={SearchScreen}
//         options={{
//           headerTitle: '',
//           headerShown: false,
//         }}
//       />
//     </HomeStack.Navigator>
//   )
// }


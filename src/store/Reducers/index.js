import { combineReducers } from 'redux';
import userReducer from '../Reducers/User Reducer';

export default combineReducers ({
  user: userReducer
})
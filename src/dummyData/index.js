let r = "Rp" 
export const fruits = [
  {
    id : 1,
    name : 'Apple',
    price : 3.500,
    image : 'https://pngimg.com/uploads/apple/apple_PNG12405.png'
  },
  {
    id : 2,
    name : 'Mango',
    price : 5.000,
    image: 'https://www.clipartmax.com/png/middle/217-2176395_alphonso-mango-png.png'
  },
  {
    id : 3,
    name : 'Orange',
    price : 3.000,
    image: 'https://banner2.cleanpng.com/20180723/tyq/kisspng-orange-juice-citrus-sinensis-stock-photography-oranges-clip-art-5b55ecfb969388.5468715515323578836168.jpg'
  },
  {
    id : 4,
    name : 'Watermelon',
    price : 15.000,
    image: 'https://www.vhv.rs/dpng/d/496-4960304_hq-watermelon-png-transparent-watermelon-images-water-melon.png'
  },
  {
    id : 5,
    name : 'Melon',
    price : 12.500,
    image: 'https://w7.pngwing.com/pngs/65/352/png-transparent-two-sliced-melons-melon-cantaloupe-fruit-melon-food-orange-watermelon.png'
  },
]

export const Vegetables = [
  {
    id : 1,
    name : 'Broccoli',
    price : 15.000
  },
  {
    id : 2,
    name : 'Cabbage',
    price : 5.000,
  },
  {
    id : 3,
    name : 'Carrot',
    price : 1.500,
  },
  {
    id : 4,
    name : 'Tomato',
    price : 1.000,
  },
  {
    id : 5,
    name : 'Water Spinach',
    price : 5.000,
  },
]
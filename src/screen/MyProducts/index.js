import React, { useEffect } from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, FlatList, Image, TouchableOpacity } from 'react-native'
import COLOR from '../../utils/Tools';
import { useSelector, useDispatch } from 'react-redux';
import { myProductsAction } from '../../../store/action/getMyProductsAction';
import MyProducts from '../../components/My Products Card';

const MyProductsScreen = ({navigation}) => {
  const data = useSelector(state => state.myProductsReducer.data)
  const userProfile = useSelector (state => state.userProfileReducer.user)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(myProductsAction(userProfile.username))
  }, [])
  console.log('My Products Screen:', data)

  const numColumns = 2;

  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />

      <View style={styles.header}>
        <Image 
          source={require('../../assets/Images/logoFix.png')}
          style={styles.logo}
        />
        <Text style={styles.title}>My Products</Text>
      </View>

      <FlatList 
        data={data}
        numColumns={numColumns}
        renderItem={({item}) => {
          console.log('My Product:', item)
          return (
            <MyProducts 
                id={item._id}
                name={item.name}
                image={item.image}
                price={item.price.$numberDecimal}
            />
          )
        }}
        keyExtractor={item => item.id}
      />

      <TouchableOpacity
        onPress={()=>navigation.navigate('Create Product')}
        style={styles.buttonSubmit}
      > 
        <Text style={styles.textSubmit}>CREATE PRODUCT</Text>
      </TouchableOpacity>
      
    </ScrollView>
  );
};

export default MyProductsScreen;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey),
  },
  containerCheckout: {
    backgroundColor: COLOR.green, 
    height: 50, 
    width: 360, 
    justifyContent: 'center', 
    alignSelf: 'center', 
    borderRadius: 10,
    marginTop: 370
  },
  titleCheckout: {
    alignSelf: 'center', 
    fontSize: 20, 
    fontWeight: 'bold', 
    color: COLOR.white
  },
  header: {
    flexDirection: 'row', 
    backgroundColor: COLOR.pureWhite,
    marginTop: 50
  },
  logo: {
    height: 60, 
    width: 60, 
    marginLeft: 10
  },
  title: {
    fontSize: 20,
    marginTop: 15,
    marginLeft: 15
  },
  buttonSubmit: { 
    width: 370, 
    height: 50, 
    backgroundColor: COLOR.green,
    color: COLOR.pureWhite,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: 15,
  },
  textSubmit: {
    color: COLOR.pureWhite,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    fontSize: 24,
    fontWeight: '500',
    paddingTop: 10
  }
})


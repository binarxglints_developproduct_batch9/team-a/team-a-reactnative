import React, {useState, useEffect} from 'react'
import {
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  Platform,
  Image,
  View,
  Text,
} from 'react-native'
import Collapsible from 'react-native-collapsible'
import COLOR from '../../utils/Tools'
import { icUp,icDown, DuBni, icCamera, icGallery } from '../../assets/Images'
import * as ImagePicker from 'react-native-image-picker'
import Modal from 'react-native-modal';
import { useSelector, useDispatch } from 'react-redux';
import { updateConfirmationPaymentAction } from '../../../store/action/UpdatePaymentAction'

const OS = Platform.OS

const Pembayaran = ({route}) => {
  const {data} = route.params;
  const [atm, setAtm] = useState(false)
  const [mbca, setMbca] = useState(true)
  const [ibank, setIbank] = useState(true)
  const [imageLaunch,setImageLaunch] = useState(false)
  const [IsSuccess,setIsSuccess] = useState(false)
  const [isImage,setIsImage] = useState('')
  const [photo,setPhoto] = useState('')

  useEffect(()=>{
    console.log("==TES==",data)
  },[])

  // actions 
  const dispatch = useDispatch()

  const status = 'dones'

  const panduan = {
    atm: [
      '1. Masukkan Kartu ATM & PIN',
      '2. Pilih "Transaksi Lainnya"',
      '3. Pilih "Transfer"',
      '4. Pilih "Ke Rek Bank Lain"',
      '5. Masukkan kode bank 014',
      '6. Masukkan nomor rekening tujuan 80770822619',
      `7. Masukkan jumlah transfer Rp ${data}`,
      '8. Baca kembali detail pembayaran dan konfirmasi transaksi'
    ],
    mbca: [
      '1. Log in pada aplikasi BCA Mobile',
      '2. Pilih menu m-BCA, kemudian masukkan kode akses m-BCA',
      '3. Pilih "m-Transfer" kemudian "Daftar Transfer - Antar Rekening"',
      '4. Masukkan nomor rekening 80770822619',
      '5. Klik "Send" dan masukkan PIN untuk mendaftarkan nomor rekening tujuan anda',
      '6. Pilih "Transfer - Antar Rekening"',
      '7. Pilih nomor rekening tujuan',
      `8. Masukkan jumlah uang Rp ${data}`,
      '9. Klik "Send" dan masukkan PIN'
    ],
    ibank: [
      '1. Login ke website Mandiri Online dengan memasukkan user ID dan PIN',
      '2. Pilih menu "Transfer"',
      '3. Lalu pilih "Ke Bank Lain"',
      '4. Masukkan kode bank 014',
      '5. Masukkan rekening tujuan 80770822619',
      `6. Masukkan jumlah transfer Rp ${data}`,
      '7. Klik Lanjut',
      '8. Periksa kembali transaksi, kemudian klik Kirim'
    ]
  }

  const atmClick = () => {
    if(!atm) {
      setAtm(true)
    } else {
      setAtm(false)
      setMbca(true)
      setIbank(true)
    }
  }

  const mbcaClick = () => {
    if(!mbca) {
      setMbca(true)
    } else {
      setAtm(true)
      setMbca(false)
      setIbank(true)
    }
  }

  const ibankClick = () => {
    if(!ibank) {
      setIbank(true)
    } else {
      setAtm(true)
      setMbca(true)
      setIbank(false)
    }
  }

  const handleCamera = (type)=>{
    if(type == 'CAMERA'){
      ImagePicker.launchCamera({}, (response) => {
        console.log('Response = ', response);

       if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          let url = {  uri: response?.uri }
          const source = { 
            uri: response.uri,
            type: response?.type,
            name: response?.fileName,
          };
          setIsImage(source)
          setPhoto(url)
        }
      });
    }else if(type == 'GALERY'){
      ImagePicker.launchImageLibrary({}, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          let url = {  uri: response?.uri }
          const source = { 
            uri: response.uri,
            type: response?.type,
            name: response?.fileName,
          };
          setIsImage(source)
          setPhoto(url)
        }
      });
    }
    setImageLaunch(false)
  }

  const HandleUploadTransaksi = ()=>{

    dispatch(updateConfirmationPaymentAction({
      idTransaction :'6014e644e0b8cb3d9972d14f',
      data: isImage
    }))
    setIsSuccess(true)
  }

  const AddImage = ()=>(
    <Modal
      isVisible={imageLaunch}
      onBackButtonPress={() => setImageLaunch(false)}
      onBackdropPress={() => setImageLaunch(false)}
      backdropTransitionOutTiming={100}
      style={{margin: 0, justifyContent: 'flex-end'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 10, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, justifyContent: 'space-between'}}>
        <View style={{alignContent: 'center', justifyContent: 'center',alignItems: 'center'}}>
          <TouchableOpacity 
          onPress={() => setImageLaunch(false)}
          activeOpacity={0.9}
          style={{width: 80, height: 5, marginTop: 5, borderRadius: 15,marginBottom: 5,backgroundColor:   "#DDDDDD"}}>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPressIn={()=>handleCamera('CAMERA')} style={{flexDirection: 'row',alignItems: 'center',marginTop: 15, marginBottom: 2}}>
          <Image source={icCamera} style={{width: 24, height: 24, marginRight: 8}}/>
          <Text>Take Picture</Text>
        </TouchableOpacity>
        <TouchableOpacity onPressIn={()=>handleCamera('GALERY')} style={{flexDirection: 'row',alignItems: 'center',marginBottom: 15, marginTop: 2}}>
          <Image source={icGallery} style={{width: 24, height: 24, marginRight: 8}}/>
          <Text>Gallery</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  )

  const ModalSuccess = ()=>(
    <Modal
      isVisible={IsSuccess}
      onBackButtonPress={() => setIsSuccess(false)}
      onBackdropPress={() => setIsSuccess(false)}
      backdropTransitionOutTiming={100}
      style={{flex: 1, justifyContent: 'center'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 5, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 15,borderBottomRightRadius: 15}}>
          <View style={{alignItems: 'center', justifyContent: 'center', marginVertical: 12}}>
            <Text style={{maxWidth: 200, textAlign: 'center',fontSize: 20}}>Confirmation Order Success !</Text>
          </View>
          <View style={{marginBottom: 20, paddingHorizontal: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
              <TouchableOpacity onPress={()=> setIsSuccess(false) } style={{backgroundColor: COLOR.blue, width: 100, marginHorizontal: 10, borderRadius: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Ok</Text>
              </TouchableOpacity>
          </View>
      </View>
    </Modal>
  )

    return (
      <SafeAreaView style={styles.pages}>
        {AddImage()}
        {ModalSuccess()}
        <StatusBar backgroundColor={COLOR.blue} barStyle='light-content' />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
          <View style={styles.myCart}>
            <Text style={styles.nominalTf}>Rp {data}</Text>
            {status == 'done' ? <View /> : <Text style={styles.desc}><Text>*</Text>Pastikan jumlah nominal yang ditransfer sama seperti yang tercantum di atas.</Text>}
          </View>
          {status == 'done' ? <View /> :
            <View>
              <View style={styles.cardShadow}>
                <Text>Transfer Pembayaran ke:</Text>
                <View style={styles.containerCard}>
                  <Text style={styles.addresBank}>1234567890 (Tarri Peritha Westi)</Text>
                  <Image source={DuBni} resizeMode="cover" style={{width: 45,height: 13}}/>
                </View>
              </View>
              <View style={{justifyContent:'center',paddingTop: 3, paddingBottom: 3, paddingHorizontal: 3,marginHorizontal: 3,marginBottom: 8,backGroundColor: 'white',borderRadius: 20}}>
                <Text>Petunjuk Pembayaran</Text>
                <View>
                  <TouchableOpacity style={styles.cardShadow} onPress={() => atmClick()}>
                    <View style={styles.containerContent}>
                      <Text style={{flex: 1, color: COLOR.blue}}>Melalui ATM</Text>
                      {
                        atm ? <Image source={icUp} style={styles.icons}/> : <Image source={icDown} style={styles.icons}/>
                      }
                    </View>
                    <Collapsible style={{paddingTop: 2}} collapsed={atm}>
                      {panduan.atm.map((item, index) => (
                        <Text key={index}>{item}</Text>
                      ))}
                    </Collapsible>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity style={styles.cardShadow} onPress={() => mbcaClick()}>
                    <View style={styles.containerContent}>
                      <Text style={{flex: 1, color: COLOR.blue}}>m-BCA (BCA Mobile)</Text>
                      {
                        mbca ? <Image source={icUp} style={styles.icons}/> : <Image source={icDown} style={styles.icons}/>
                      }
                    </View>
                    <Collapsible style={{paddingTop: 2}} collapsed={mbca}>
                      {panduan.mbca.map((item, index) => (
                        <Text key={index}>{item}</Text>
                      ))}
                    </Collapsible>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity style={styles.cardShadow} onPress={() => ibankClick()}>
                    <View style={styles.containerContent}>
                      <Text style={{flex: 1, color: COLOR.blue}}>Internet Banking</Text>
                      {
                        ibank ? <Image source={icUp} style={styles.icons}/> : <Image source={icDown} style={styles.icons}/>
                      }
                    </View>
                    <Collapsible style={{paddingTop: 2}} collapsed={ibank}>
                      {panduan.ibank.map((item, index) => (
                        <Text key={index}>{item}</Text>
                      ))}
                    </Collapsible>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{height: 200 , marginBottom: 10,backgroundColor: '#e3e3e3',marginHorizontal: 10, borderBottomLeftRadius: 5,borderBottomRightRadius: 5,borderTopLeftRadius: 5,borderTopRightRadius: 5, paddingTop: 10}}>
                { photo ? (
                  <View style={{alignItems: 'center',justifyContent: 'center',marginTop: 15}}>
                    <Image source={photo && photo } style={{width: 300, height: 150}} resizeMode="cover"/>
                  </View>
                ) : (
                  <TouchableOpacity onPressIn={()=> setImageLaunch(true)} style={{alignItems: 'center',justifyContent: 'center',flex: 1}}>
                    <Image source={icCamera} style={{width: 24, height: 24}}/>
                    <Text>Upload</Text>
                  </TouchableOpacity>
                )
                }{
                  photo ? (
                    <TouchableOpacity onPress={()=> setPhoto('')} style={{backgroundColor: 'red', width: 20, height: 20, borderRadius: 999,alignItems: 'center',alignSelf: 'flex-end',marginRight: 15, position: 'absolute',marginTop: 10}}>
                      <Text style={{color: 'white'}}>X</Text>
                    </TouchableOpacity>
                  ) : null
                }
              </View>
            </View>
          }
        </ScrollView>
        <View style={{justifyContent: 'flex-end',backgroundColor: 'white'}}>
          <TouchableOpacity onPress={HandleUploadTransaksi} style={{alignItems: 'center',paddingHorizontal: 3,paddingVertical: 3, backgroundColor: COLOR.blue, marginHorizontal: 3, borderRadius: 10}} activeOpacity={0.9}>
            <Text style={{color: 'white',marginVertical: 10}}>Confirm Payment</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }

export default Pembayaran
const styles = StyleSheet.create({
  pages: {marginHorizontal: 5, flex: 1,backgroundColor: 'white',paddingVertical: 40},
  container: {marginVertical: 5,backgroundColor: 'white'},
  myCart: {borderWidth: 1,borderColor: "0 4 4 rgba(0, 0, 0, 0.15)",borderBottomWidth: 3,
  borderRadius: 5, paddingHorizontal: 10,paddingVertical: 10, marginVertical: 3},
  nominalTf: {color: 'red', marginBottom: 3, textAlign: 'center'},
  desc: {marginHorizontal: 3, fontSize: 15, fontStyle: 'italic'},
  cardShadow: {borderWidth: 1,borderColor: "0 4 4 rgba(0, 0, 0, 0.15)",borderBottomWidth: 3,
  borderRadius: 5, paddingHorizontal: 10,paddingVertical: 10, marginVertical: 3},
  containerCard: {borderWidth: 1,borderColor: "0 4 4 rgba(0, 0, 0, 0.15)",borderBottomWidth: 3,
  borderRadius: 5, paddingHorizontal: 10,paddingVertical: 10, marginVertical: 3, flexDirection: 'row',alignItems: 'center'},
  addresBank: {fontWeight: 'bold',fontSize: 14, color: COLOR.blue, flex: 1},
  containerContent: {flex: 1,flexDirection: 'row',alignItems: 'center'},
  icons: {width: 24, height: 24},
})
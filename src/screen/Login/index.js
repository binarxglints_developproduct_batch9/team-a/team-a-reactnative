import React, { useEffect, useState } from 'react'
import { 
  StyleSheet, Text, 
  View, StatusBar, 
  TextInput,
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

import axios from '../../helper/axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import COLOR from '../../utils/Tools';

const Login = ({navigation}) => {

  const [email,setEmail] = useState('')
  const [password,setPassword] = useState('')

  useEffect(() => {

    AsyncStorage.getItem('access_token', (err,res) => {
      if (err) console.log('access token not defined AsyncStorage', err)
      // else console.log('res AsyncStorage', res) //melihat isi dari AsyncStorage
      else if (res) navigation.navigate('Home')
    })
    // let loggedIn = AsyncStorage.getItem('access_token')
    // console.log('loggedIn', loggedIn)
    // loggedIn
    //   .then(_ => navigation.navigate('Home'))
    //   .catch(console.log)
  }, [])

  const [ data, setData ] = useState ({
    email: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidEmail: true,
    isValidPassword: true,
  });

  const textUserInputChange = (val) => {
    if( val.length !== 0) {
      setData({
        ...data,
        email: val,
        check_textUserInputChange: true
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textUserInputChange: false
      });
    }
  };

  const textInputChange = (val) => {
    if( val.trim().length >= 4) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true,
        isValidEmail: true
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
        isValidEmail: false
      });
    }
  };

  const handlePasswordChange = (val) => {
    if( val.trim().length >= 8 ){
      setData({
        ...data,
        password: val,
        isValidPassword: true
    });
  } else {
    setData({
      ...data,
      password: val,
      isValidPassword: false
  });
  }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
    });
  };

  const handleValidEmail = (val) => {
    if(val.trim().length >= 4) {
      setData({
        ...data,
        isValidEmail: true
      });
    } else {
      setData({
        ...data,
        isValidEmail: false
      });
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar 
        backgroundColor={(COLOR.white)} 
        barStyle='light-content'
      />
      <Animatable.Image
        animation="bounceIn"
        source={require('../../assets/Images/logoFix.png')}
        style={styles.logo}
      />
      <View style={styles.containerLogin}>
        <View style={styles.login}>
        <View style={styles.action}>
        <FontAwesome 
            name="envelope"
            color= {COLOR.green}
            size={20}
            style={{ marginTop: 10 }}
          />
          <TextInput 
            placeholder='Email'
            style={{ marginLeft: 5 }}
            autoCapitalize= 'none'
            value={email}
            onChangeText={(text) => {setEmail(text)}}
            onEndEditing={(e) => handleValidEmail(e.nativeEvent.text)}
          />
          </View>
          { data.isValidEmail ? null :
          <Animatable.View animation="fadeInLeft" duration={500}>
          <Text style={ styles.errorMsg }>The email you entered is wrong</Text>
          </Animatable.View>
          }

          <View style={styles.action}>
          <FontAwesome 
            name="lock"
            color= {COLOR.green}
            size={22}
            style={{ marginTop: 10}}
          />
          <TextInput 
            placeholder='Password'
            style={{ marginLeft: 5 }}
            autoCapitalize="none"
            secureTextEntry={true}
            value={password}
            onChangeText={(text) => {setPassword(text)}}
          />
          </View>
          {data.isValidPassword ? null : 
          <Animatable.View animation="fadeInLeft" duration={500}>
          <Text style={ styles.errorMsg }>Password must be 8 characters long</Text>
          </Animatable.View>
          }
          <TouchableOpacity>
            <Text 
              style={styles.button}>Forgot Password?
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
                axios({
                  method: 'POST',
                  url: '/login',
                  data: {
                    emailOrUsername: email,
                    password: password
                  }
                })
                  .then(({data}) => {
                    AsyncStorage.setItem('access_token', data.token)
                    // console.log(data)
                    navigation.navigate('Home')
                    
                  })
                  .catch(err => {console.log('ini error di login', err.message)})
              }}>
            <Text
              style={styles.button1}>Login
            </Text>
          </TouchableOpacity>
          <View style={styles.register}>
            <Text>Don't have an account?</Text>
            <TouchableOpacity onPress={()=> navigation.navigate('Register')}>
              <Text style={styles.text}>Register</Text>
            </TouchableOpacity>
            </View>
        </View>
      </View>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: COLOR.white
  },
  logo: {
    marginTop: -150,
    height: 150,
    width: 150
  },
  containerLogin:{
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: COLOR.white,
    width: 350,
    height: 400,
    borderRadius: 30,
    marginTop: -10
  },
  login:{
    width: 300,
    height: 100,
    marginTop: -200,
    borderBottomColor: COLOR.green,
    paddingHorizontal: 20,
  },
  action:{
    flexDirection: 'row',
    borderColor: COLOR.green,
    borderBottomWidth: 1,
    marginTop: 10
  },
  button: {
    alignSelf: 'flex-end',
    color: COLOR.red,
    marginTop: 15
  },
  button1:{
    backgroundColor: COLOR.green,
    fontSize: 21,
    fontWeight: '700',
    color: COLOR.white,
    width: 200,
    height: 35,
    marginTop: 30,
    borderRadius: 10,
    alignSelf: 'center',
    textAlign: 'center',
  },
  register:{
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 50
  },
  text:{
    color: COLOR.green,
    marginLeft: 5
  },
  errorMsg: {
    color: COLOR.red,
    fontSize: 14,
  },
})

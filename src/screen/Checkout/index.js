import React from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, TouchableOpacity, Image  } from 'react-native'
import COLOR from '../../utils/Tools';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const CheckoutScreen = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={{backgroundColor: COLOR.pureWhite, marginTop: 50, height: 50, flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <FontAwesome name='arrow-left' size={25} color={COLOR.green}
            style={{marginLeft: 15, marginTop: 10}}
          />
        </TouchableOpacity>
          <Text style={{fontSize: 20, marginLeft: 20, marginTop: 10}}>Checkout</Text>
      </View>
      <View style={{backgroundColor: COLOR.pureWhite, marginTop: 15, height: 300, paddingLeft: 15}}>
        <Text style={{fontSize: 18, fontWeight: '700', marginTop: 10}}>Products List</Text>
        <View style={{borderWidth: 0.7, borderColor: COLOR.green, width: 360, height: 200, marginTop: 5}}>
          <Image source={require('../../assets/Images/Pisang.png')}
            style={{height: 150, width: 150}}
          />
          <Text>Pisang Raja</Text>
          <Text>Rp 25.000,-</Text>
          <Text>Rp 25.000,-</Text>
          <Text>QTY 1</Text>
          
        </View>
      </View>
      
    </ScrollView>
  );
};

export default CheckoutScreen;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
})


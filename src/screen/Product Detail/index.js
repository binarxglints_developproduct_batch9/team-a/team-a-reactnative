import React, { useCallback, useEffect, useState } from 'react'
import { 
  TouchableOpacity,
  LayoutAnimation,
  SectionList,
  StyleSheet,
  ScrollView, 
  Dimensions,
  TextInput,
  StatusBar,
  UIManager, 
  FlatList,
  Button,
  Image, 
  Text, 
  View, 
} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Modal from 'react-native-modal';
import { Rating, AirbnbRating } from 'react-native-elements';

import COLOR from '../../utils/Tools';
import ProductDetailCard from '../../components/Product Detail Card';
import qs from "qs";

import { BASE_URL } from '../../../store/constant/general';

import { storeReviewAction } from '../../../store/action/storeReviewAction';
import { getReviewAction } from '../../../store/action/getReviewAction';
import { addTocartAction } from '../../../store/action/addTocartAction';
import { useSelector, useDispatch } from 'react-redux';
import { updateReviewAction } from '../../../store/action/updateReviewAction';
import { getDiscusionAction } from '../../../store/action/getDiscusionAction';
import {  postDiscusionAction } from '../../../store/action/postDiscusionAction'
import { putDiscusionAction } from '../../../store/action/putDiscusionAction';

const { width } = Dimensions.get('window')

const DetailProductScreen = ({route, id, name, image, price, description, navigation}) => {
  
  console.log("detail product", detail)
  const detail = useSelector(state => state.detailProductReducer.detail)
  const review = useSelector(state => state.getReviewReducer)
  const discusion = useSelector(state => state.getDiscusionReducer)
  const { loading, reviews } = review
  const cart = useSelector(state => state.cartItemsReducer.items)
  const dispatch = useDispatch();
  const [idComments,setIdComments] = useState('')
  // Inputs
  const [addRating,setAddRating] = useState('')
  const [addReview,setAddReview] = useState('')
  const [rating,setRating] = useState('')
  const [comment,setComment] = useState('')
  const [idBuyer,setIdBuyer] = useState('')
  const [addQuest,setAddQuest] = useState('')
  const [replyQuest,setReplyQuest] = useState('')

  // modal
  const [modalreview,setModalreview] = useState(false)
  const [editreview,seteditreview] = useState(false)
  const [modalDiscuus,setModalDiscus] = useState(false)
  const [replyDiscus,setReplyDiscus] = useState(false)
  
  // Tab Satate
  const [activeTab, setActiveTab] = useState(0)
  const [xTabOne, setXTabOne] = useState(0)
  const [xTabTwo, setXTabTwo] = useState(0)


  useEffect(() => {
    dispatch(getReviewAction(detail?._id))
    dispatch(getDiscusionAction(detail?._id))
    console.log("Dtaa Revwe",detail?._id)
  },[detail])

  if (
    Platform.OS === "android" &&
    UIManager.setLayoutAnimationEnabledExperimental
  ) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  const toggleTab = tabSelected => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    setActiveTab(tabSelected)
  }

const _HandleSendReview = useCallback(()=>{
  
  dispatch(storeReviewAction({
    product_id : detail?._id,
    data : qs.stringify({
      comment: addReview,
      rating: addRating,
    })
  }))
  setAddReview('')
  setAddRating('')
  setModalreview(false)
},[addReview,addRating])

const handleAddItemToCart = useCallback(()=>{
  dispatch(addTocartAction({productId: detail?._id}))
  navigation.navigate('Cart')
},[detail])

const HandleUpdateReview = (item)=>{
  setComment(item?.comment)
  setRating(item?.rating?.$numberDecimal)
  setIdBuyer(item?._id)
  seteditreview(true)
}

const _HandleUpdateReview = useCallback(()=>{
  seteditreview(false)
  dispatch(updateReviewAction({
    idproduct : detail?._id,
    idbuyer : idBuyer,
    data : qs.stringify({
      comment: comment,
      rating: rating
    })
  }))
},[comment,rating,idBuyer])

const _HandleSendQuestion = ()=> {
  dispatch(postDiscusionAction({
    param : detail?._id,
    data : qs.stringify({
      question: addQuest
    })
  }))
  setAddQuest('')
  setModalDiscus(false)
}

const _HandleReplyDisuce = useCallback(()=> {
  const data = {
    idComent : idComments,
    param : detail?._id,
    data: qs.stringify({
      comment: replyQuest
    })
  }
  dispatch(putDiscusionAction(data))
  console.log("=== REPLY DATA ===",replyQuest)
  setReplyQuest('')
  setReplyDiscus(false)
},[idComments,replyQuest])

const ModaUpdate = ()=>(
    <Modal
      isVisible={editreview}
      onBackButtonPress={() => seteditreview(false)}
      onBackdropPress={() => seteditreview(false)}
      backdropTransitionOutTiming={100}
      useNativeDriver={true}
      style={{margin: 0, justifyContent: 'flex-end'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 5, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, justifyContent: 'space-between'}}>
        <View style={{alignContent: 'center', justifyContent: 'center',alignItems: 'center'}}>
          <TouchableOpacity 
          onPress={() => seteditreview(false)}
          activeOpacity={0.9}
          style={{width: 80, height: 5, marginTop: 5, borderRadius: 15,marginBottom: 5,backgroundColor:   "#DDDDDD"}}>
          </TouchableOpacity>
        </View>
          <View style={{marginBottom: 20, paddingHorizontal: 5}}>
            <View>
              <Text style={{marginHorizontal: 15,color: 'black',marginTop: 10}}>Input Rating</Text>
              <TextInput
                style={{borderWidth: 1, borderColor: "gray", borderRadius: 10, marginVertical: 12, marginHorizontal: 10, width: 100, paddingVertical: 2}}
                value={rating}
                onChangeText={val=> setRating(val)}
                placeholder="Input Rating"
              />
              <Text style={{marginHorizontal: 15}}>Input Your Reviews</Text>
              <TextInput
                style={{borderWidth: 1, borderColor: "gray", borderRadius: 10, marginVertical: 12, marginHorizontal: 10 }}
                value={comment}
                onChangeText={val=> setComment(val)}
                placeholder="Input Review"
              />
              <TouchableOpacity onPress={_HandleUpdateReview} style={{backgroundColor: COLOR.    green, marginHorizontal: 10, borderRadius: 20,marginVertical: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Update Review</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    </Modal>
  )
const AddReviews = ()=>(
    <Modal
      isVisible={modalreview}
      onBackButtonPress={() => setModalreview(false)}
      onBackdropPress={() => setModalreview(false)}
      backdropTransitionOutTiming={100}
      style={{margin: 0, justifyContent: 'flex-end'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 5, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, justifyContent: 'space-between'}}>
        <View style={{alignContent: 'center', justifyContent: 'center',alignItems: 'center'}}>
          <TouchableOpacity 
          onPress={() => setModalreview(false)}
          activeOpacity={0.9}
          style={{width: 80, height: 5, marginTop: 5, borderRadius: 15,marginBottom: 5,backgroundColor:   "#DDDDDD"}}>
          </TouchableOpacity>
        </View>
          <View style={{marginBottom: 20, paddingHorizontal: 5}}>
            <View>
              <Text style={{marginHorizontal: 15,color: 'black',marginTop: 10}}>Input Rating</Text>
              <TextInput
                style={{borderWidth: 1, borderColor: "gray", borderRadius: 10, marginVertical: 12, marginHorizontal: 10, width: 100, paddingVertical: 2}}
                value={addRating}
                onChangeText={val=> setAddRating(val)}
                placeholder="Input Rating"
              />
              <Text style={{marginHorizontal: 15}}>Input Your Reviews</Text>
              <TextInput
                style={{borderWidth: 1, borderColor: "gray", borderRadius: 10, marginVertical: 12, marginHorizontal: 10 }}
                value={addReview}
                onChangeText={val=> setAddReview(val)}
                placeholder="Input Review"
              />
              <TouchableOpacity onPress={ _HandleSendReview} style={{backgroundColor: COLOR.    green, marginHorizontal: 10, borderRadius: 20,marginVertical: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    </Modal>
  )
const AddDiscus = ()=>(
    <Modal
      isVisible={modalDiscuus}
      onBackButtonPress={() => setModalDiscus(false)}
      onBackdropPress={() => setModalDiscus(false)}
      useNativeDriver={true}
      backdropTransitionOutTiming={100}
      style={{margin: 0, justifyContent: 'flex-end'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 5, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, justifyContent: 'space-between'}}>
        <View style={{alignContent: 'center', justifyContent: 'center',alignItems: 'center'}}>
          <TouchableOpacity 
          onPress={() => setModalDiscus(false)}
          activeOpacity={0.9}
          style={{width: 80, height: 5, marginTop: 5, borderRadius: 15,marginBottom: 5,backgroundColor:   "#DDDDDD"}}>
          </TouchableOpacity>
        </View>
          <View style={{marginBottom: 20, paddingHorizontal: 5}}>
            <View>
              <Text style={{marginHorizontal: 15}}>Input Your Question ?</Text>
              <TextInput
                style={{borderWidth: 1, borderColor: "gray", borderRadius: 10, marginVertical: 12, marginHorizontal: 10}}
                value={addQuest}
                onChangeText={val=> setAddQuest(val) }
                placeholder="Input Question"
                multiline={true}
              />
              <TouchableOpacity onPress={_HandleSendQuestion} style={{backgroundColor: COLOR.    green, marginHorizontal: 10, borderRadius: 20,marginVertical: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    </Modal>
  )
const ReplyDiscus = ()=>(
    <Modal
      isVisible={replyDiscus}
      onBackButtonPress={() => setReplyDiscus(false)}
      onBackdropPress={() => setReplyDiscus(false)}
      backdropTransitionOutTiming={100}
      useNativeDriver={true}
      style={{margin: 0, justifyContent: 'flex-end'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 5, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, justifyContent: 'space-between'}}>
        <View style={{alignContent: 'center', justifyContent: 'center',alignItems: 'center'}}>
          <TouchableOpacity 
          onPress={() => setReplyDiscus(false)}
          activeOpacity={0.9}
          style={{width: 80, height: 5, marginTop: 5, borderRadius: 15,marginBottom: 5,backgroundColor:   "#DDDDDD"}}>
          </TouchableOpacity>
        </View>
          <View style={{marginBottom: 20, paddingHorizontal: 5}}>
            <View>
              <Text style={{marginHorizontal: 15}}>Reply Question </Text>
              <TextInput
                style={{borderWidth: 1, borderColor: "gray", borderRadius: 10, marginVertical: 12, marginHorizontal: 10}}
                value={replyQuest}
                onChangeText={val=> setReplyQuest(val)}
                placeholder="Input reply Question"
                multiline={true}
              />
              <TouchableOpacity onPress={_HandleReplyDisuce} style={{backgroundColor: COLOR.    green, marginHorizontal: 10, borderRadius: 20,marginVertical: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    </Modal>
  )

  const HandleRating = (item)=>{
    switch(parseInt(item?.rating?.$numberDecimal)){
      case 1:
        return <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>;
      case 2:
        return(
          <>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
          </>
        )
      case 3:
        return(
          <>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
          </>
        )
      case 4:
        return(
          <>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
          </>
        )
      case 5:
        return(
          <>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
            <Image source={require('../../assets/Images/star.png')} style={{width: 15, height: 15}}/>
          </>
        )
      default: 
      null
    }
  }

const RenderDiscus = ({item})=>{
  console.log(item)
  return(
    <View>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <Image source={{uri: 'http://placeimg.com/80/80/people/sepia'}} style={{width: 40, height: 40, borderRadius: 30}}/>
        <Text style={{marginTop: 10, fontSize: 16, marginLeft: 5}}>{item?.commentator?.username}</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 40}}>
        {HandleRating(item)}
      </View>
      <TouchableOpacity onPress={()=>{
        setIdComments(item?._id)
        setReplyDiscus(true)
      }} style={{backgroundColor: COLOR.white,borderWidth: 0.7, borderColor: COLOR.green ,width: 300, height: 50, borderRadius: 15, marginLeft: 40, marginTop: 10}}>
        <View style={{marginTop: 10, paddingLeft: 10}}>
          <Text>{item?.question}</Text>
        </View>
      </TouchableOpacity>
      {
        item?.answer?.map((el)=>{
          return(
            <View style={{paddingLeft: 60,marginVertical: 1}}>
              <View style={{flexDirection: 'row', marginVertical: 1, alignItems: 'center'}}>
                <Image source={{uri: 'http://placeimg.com/80/80/people/sepia'}} style={{width: 24, height: 24, borderRadius: 20}}/>
                <Text style={{marginTop: 10, fontSize: 16, marginLeft: 5}}>{el?.username}</Text>
              </View>
                <View style={{backgroundColor: COLOR.white,borderWidth: 0.7, borderColor: COLOR.green, height: 50, borderRadius: 15, marginTop: 10,marginRight: 20}}>
                  <View style={{marginTop: 10, paddingLeft: 10}}>
                    <Text>{el?.comment}</Text>
                  </View>
                </View>
            </View>
          )
        })
      }
    </View>
  )
}   
const RenderViews = ({item})=>{
  console.log(item)
  return(
    <>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <Image source={{uri: 'http://placeimg.com/80/80/people/sepia'}} style={{width: 40, height: 40, borderRadius: 30}}/>
        <Text style={{marginTop: 10, fontSize: 16, marginLeft: 5}}>{item?.buyer?.username}</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 40}}>
        {HandleRating(item)}
      </View>
      <TouchableOpacity onPress={()=> HandleUpdateReview(item)} style={{backgroundColor: COLOR.white,borderWidth: 0.7, borderColor: COLOR.green ,width: 300, height: 50, borderRadius: 15, marginLeft: 40, marginTop: 10}}>
        <View style={{marginTop: 10, paddingLeft: 10}}>
          <Text>{item?.comment}</Text>
        </View>
      </TouchableOpacity>
    </>
  )
}   

if(typeof detail.seller == 'undefined') {
  return null
}

  return (
    <ScrollView style={styles.container}>
      {AddReviews()}
      {ModaUpdate()}
      {AddDiscus()}
      {ReplyDiscus()}
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <FontAwesome 
            name='arrow-left' 
            size={25} 
            color={COLOR.green}
            style={{marginLeft: 15, marginTop: 10}}
          />
        </TouchableOpacity>

          <View style={styles.headerRight}>
            <TouchableOpacity 
              style={{marginRight: 10, marginTop: 3}}
              onPress={() => navigation.navigate('Cart')}
              >
              <FontAwesome 
                name='shopping-basket'
                color={COLOR.green}
                size={23}
              />
            </TouchableOpacity>
            <Text style={{marginLeft: -15, marginTop: -6, fontSize: 20, backgroundColor: COLOR.orange, borderRadius: 30, width: 25, height: 25, textAlign: 'center', color:COLOR.black}}>0</Text>
            <TouchableOpacity 
              style={{marginLeft: 10}}
              onPress={() => navigation.navigate('Home')}
              >
              <FontAwesome 
                name='home'
                color={COLOR.green}
                size={30}
              />
            </TouchableOpacity>
          </View>
      </View>
      
      <ProductDetailCard
        id={route.params.id}
        image={route.params.image}
        name={route.params.name}
        price={route.params.price}
        description={route.params.desc}
      />
      <>
      <View style={{backgroundColor: COLOR.pureWhite, marginTop: 5, paddingLeft: 15, height: 200, paddingTop: 10}}>
        <View style={{flexDirection: 'row', borderBottomWidth: 0.5,borderBottomColor: COLOR.green, paddingBottom: 10}}>
          <Image 
            // source={{uri: 'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg'}}
            source={{uri: `${BASE_URL}${detail.seller.image}`}}
            style={{height: 40, width: 40, borderRadius: 30}}
          />
          <Text style={{fontSize: 20, marginTop: 7, marginLeft: 20}}>{detail.seller.username}</Text>
          {/* <TouchableOpacity style={{backgroundColor: COLOR.green, width: 100, height: 25, borderRadius: 5, justifyContent: 'center', alignItems: 'center', marginTop: 10, marginRight: 15}}>
            <Text style={{color: COLOR.pureWhite}}>Detail Seller</Text>
          </TouchableOpacity> */}
        </View>
        <View>
          <Text style={{ fontSize: 16, marginTop: 10, fontWeight: '800'}}>Product Detail :</Text>
          <Text style={{marginTop: 5}}>Stock: {detail.stock.$numberDecimal}</Text>
          <Text style={{marginTop: 5}}>Category: {detail.category} </Text>
          <Text style={{marginTop: 5}}>Description :</Text>
          <Text style={{marginTop: 5}}>{detail.desc}</Text>
        </View>
      </View>
      </>
      <View style={{flex: 1,backgroundColor: 'white',padding: 10}}>
        <View style={{backgroundColor: 'white',paddingBottom: 6, marginTop: 3}}>
            <View style={{flexDirection: 'row',alignItems: 'center',borderRadius: 8,backgroundColor: COLOR.green,marginHorizontal: 3,paddingVertical: 4, paddingHorizontal: 4,borderBottomWidth: 1,borderColor: '#fafafa'}}>
              <View
                style={[
                  {position: 'absolute',width: "50%",height: "100%",paddingVertical: 4,paddingHorizontal: 4,backgroundColor: 'white',borderRadius: 8,borderTopWidth: 1 },
                  activeTab === 0 && { left: xTabOne + 1 },
                  activeTab === 1 && { left: xTabTwo - 1 },
                ]}
              />
              <TouchableOpacity
                activeOpacity={0.9}
                style={{flex:1, paddingHorizontal: 4,paddingVertical: 4, alignItems: 'center',justifyContent: 'center'}}
                onLayout={event => setXTabOne(event.nativeEvent.layout.x)}
                onPress={() => toggleTab(0)}
              >
                <Text style={[
                  {textAlign: 'center', paddingVertical: 4,color: 'white'},
                  activeTab === 0 && {color: COLOR.green}
                ]}>Review</Text>
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.9}
                style={{flex: 1,textAlign: 'center', paddingVertical: 4,paddingHorizontal: 4,color: 'black'}}
                onLayout={event => setXTabTwo(event.nativeEvent.layout.x)}
                onPress={() => toggleTab(1)}
              >
                <Text style={[
                  {textAlign: 'center', paddingVertical: 4,color: 'white'},
                  activeTab === 1 && {color: COLOR.green}
                ]}>Discussion</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={[
            { width: width * 2, flex: 1, flexDirection:'row' },
            activeTab === 0 && { transform: [{ translateX: 0 }] },
            activeTab === 1 && { transform: [{ translateX: -width }] },
          ]}>
            <View style={{flex: 1}}>
              <View style={{flexDirection: 'row', alignItems: 'center',}}>
                <Text style={{marginTop: 10, fontSize: 20, fontWeight: '600',flex: 1}}>Review</Text>
                <TouchableOpacity onPress={()=> setModalreview(true) } style={{backgroundColor: COLOR.green, borderRadius: 5,marginRight: 20}}>
                  <Text style={{marginVertical: 3, color: 'white',marginHorizontal: 10}}>Add Review</Text>
                </TouchableOpacity>
              </View>
              {
                loading ? ( 
                  <>
                  <SkeletonPlaceholder>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                      <View style={{ width: 40, height: 40, borderRadius: 50 }} />
                      <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 80, height: 15, borderRadius: 4 }} />
                        <View
                          style={{ marginTop: 6, width: 150, height: 15, borderRadius: 4 }}
                        />
                      </View>
                    </View>
                  </SkeletonPlaceholder>
                  <SkeletonPlaceholder>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                      <View style={{ width: 40, height: 40, borderRadius: 50 }} />
                      <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 80, height: 15, borderRadius: 4 }} />
                        <View
                          style={{ marginTop: 6, width: 150, height: 15, borderRadius: 4 }}
                        />
                      </View>
                    </View>
                  </SkeletonPlaceholder>
                  </>
                 ) : (
                   <>
                    <FlatList
                      data={reviews}
                      keyExtractor={(item,index)=> index.toString()}
                      renderItem={({item})=> <RenderViews item={item}/>}
                    />
                   </>
                 )
              }
            </View>         
            <View style={{flex: 1}}>
              <View style={{flexDirection: 'row', alignItems: 'center',}}>
                <Text style={{marginTop: 10, fontSize: 20, fontWeight: '600',flex: 1}}>Discussion</Text>
                <TouchableOpacity onPress={()=> setModalDiscus(true) } style={{backgroundColor: COLOR.green, borderRadius: 5,marginRight: 20}}>
                  <Text style={{marginVertical: 3, color: 'white',marginHorizontal: 10}}>Add Discuse</Text>
                </TouchableOpacity>
              </View>
              {
                discusion?.loading ? ( 
                  <>
                  <SkeletonPlaceholder>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                      <View style={{ width: 40, height: 40, borderRadius: 50 }} />
                      <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 80, height: 15, borderRadius: 4 }} />
                        <View
                          style={{ marginTop: 6, width: 150, height: 15, borderRadius: 4 }}
                        />
                      </View>
                    </View>
                  </SkeletonPlaceholder>
                  <SkeletonPlaceholder>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                      <View style={{ width: 40, height: 40, borderRadius: 50 }} />
                      <View style={{ marginLeft: 20 }}>
                        <View style={{ width: 80, height: 15, borderRadius: 4 }} />
                        <View
                          style={{ marginTop: 6, width: 150, height: 15, borderRadius: 4 }}
                        />
                      </View>
                    </View>
                  </SkeletonPlaceholder>
                  </>
                 ) : (
                   <>
                    <FlatList
                      data={discusion?.data?.data}
                      keyExtractor={(item,index)=> index.toString()}
                      renderItem={({item})=> <RenderDiscus item={item}/>}
                    />
                   </>
                 )
              }
            </View>
          </View>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginTop: 30}}>
        <View style={{}}>
          <TouchableOpacity 
            style={{backgroundColor: COLOR.green, height: 35, width: 120, justifyContent: 'center', alignItems: 'center', borderRadius: 5, marginLeft: 20, flexDirection: 'row' }}
            onPress={handleAddItemToCart}>
            <FontAwesome name="cart-plus" size={25} color={COLOR.white}/>
            <Text style={{color: COLOR.white, marginLeft: 3}}>ADD TO CART</Text>
          </TouchableOpacity>
        </View>
        
        <TouchableOpacity style={{backgroundColor: COLOR.orange, height: 35, width: 120, justifyContent: 'center', alignItems: 'center', borderRadius: 5, marginRight: 20, flexDirection: 'row' }}
          onPress={() => navigation.navigate('Checkout')}
        >
          <Text style={{fontWeight: '600', color: COLOR.black}}>BUY NOW</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default DetailProductScreen;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
  header: {
    backgroundColor: COLOR.pureWhite, 
    marginTop: 50, 
    height: 50, 
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  headerRight: {
    flexDirection: 'row', 
    marginRight: 15, 
    marginTop: 10
  }
})
import React, {useState} from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ImagePicker from 'react-native-image-picker'
import editImage from '../assets/Images/fruits.png'


const imagePicker = () => {
  const [image, setImage] = useState(null)

  return (
    <View style={{alignItems: 'center', justifyContent: 'center'}}>
    <TouchableOpacity  onPress={()=>{ 
                      const options = {
                        title: 'Select Avatar',
                        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
                        storageOptions: {
                          skipBackup: true,
                          path: 'images',
                        },
                      };
                      ImagePicker.showImagePicker(options, (response) => {
                        console.log('Response = ', response);
                                
                                    if (response.didCancel) {
                                    console.log('User cancelled image picker');
                
                                } else if (response.error) {
                                    console.log('ImagePicker Error: ', response.error);
                                    } else if (response.customButton) {
                                    console.log('User tapped custom button: ', response.customButton);
                                    } else {
                                    const source = { uri: response.uri };
                            setImage(source)

                        }
                      })
                     }}
                     >
                        <Image source={editImage} style={styles.editImage}/>
                     {/* <Text>klik sini</Text> */}
                </TouchableOpacity>
      <Text>hai</Text>
    </View>
  )
}

export default imagePicker

const styles = StyleSheet.create({
  editImage : {
    left:220,
    height:50,
    width:50,     
},

})

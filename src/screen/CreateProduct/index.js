import React from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, Image,} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Avatar, Caption, Title } from 'react-native-paper'
import COLOR from '../../utils/Tools';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { TextInput } from 'react-native-paper';

import Header from '../../components/Header';
import Form from '../../components/Form';

const EditProductScreen = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={{backgroundColor: COLOR.pureWhite, marginTop: 50, height: 50, flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <FontAwesome name='arrow-left' size={25} color={COLOR.green}
            style={{marginLeft: 15, marginTop: 10}}
          />
        </TouchableOpacity>
          <Text style={{fontSize: 20, marginLeft: 20, marginTop: 10}}>Create Product</Text>
      </View>
      <View style={{backgroundColor: COLOR.pureWhite, height: 750, marginTop: 15,}}>
        {/* <View >
          <View style={{backgroundColor: COLOR.grey, height: 130, width: 130, borderRadius: 10,  marginTop: 15, marginLeft: 15, flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={()=>{}}
              style={{marginLeft: 220, width: 130, height: 40, marginTop: 50, borderWidth: 1, borderColor: COLOR.green, borderRadius: 5, backgroundColor: COLOR.green, justifyContent: 'center'}}
            >
              <Text style={{ alignItems: 'center', justifyContent: 'center', alignSelf: 'center', color: COLOR.pureWhite, fontSize: 16}}>Change Images</Text>
            </TouchableOpacity>
          </View>
        </View> */}
        <Form />
      </View>
    </ScrollView>
  );
};

export default EditProductScreen;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey),
  },
})


import React, { useCallback, useEffect, useState } from 'react'
import { StyleSheet, Text, View, StatusBar, ScrollView, Button, Image, TouchableOpacity } from 'react-native'
import { FlatList } from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import COLOR from '../../utils/Tools';
import * as types from '../../../store/constant/actionTypes';
import Modal from 'react-native-modal';

import { useDispatch, useSelector } from 'react-redux';
import { getCartAction } from '../../../store/action/getCartAction';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { BASE_URL } from '../../../store/constant/general'
import { destroyCartAction } from '../../../store/action/destroyCardAction';
import { CreateTransactionAction } from '../../../store/action/CreateTransactionAction';
import { addItemToCart } from '../../../store/action/cartAction';

const Cart = (props) => {

  let dispatch = useDispatch()
  const counter = useSelector ( state => state.cartItemsReducer.counter )
  const carts = useSelector(state => state.cartItemsReducer.items)
  const myCart = useSelector(state => state.getCartReducer)
  const myMoneys = useSelector(state => state.cartItemsReducer) 
  const [ModalDelete,setModalDelete] = useState(false)
  const [idCart,setIdCart] = useState('')
  const [myMoneyCart,setMayMoneyCart] = useState('')
  
  const { loading, cart } = myCart

  const PriceForm = (num) => {
    let str = num.toString(),
    split = str.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr( 0, sisa ),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  useEffect (() => {
    dispatch(getCartAction())
    let newPrice = 0;
    !loading && cart?.data?.map((item)=>{
      const hasil = myMoneys?.counter * parseInt (item?.productId?.price?.$numberDecimal)
      newPrice += hasil
    })
    console.log(myMoneys?.counter)
    setMayMoneyCart(newPrice)    
  },[myMoneys?.counter])

  const HanldeDestroyCartAction = useCallback(()=>{
    dispatch(destroyCartAction(idCart))
    console.log("Idcart",idCart)
    setModalDelete(false)
  },[cart,idCart])


  const HanldeDestroyCart = (id) =>{
    setIdCart(id)
    setModalDelete(true)
  }

  const SubTotal = useCallback(()=>{
    return(
      <View style={styles.containerTotal}>
        <Text style={styles.titleTotal}>SubTotal: </Text>
        <Text style={styles.totalPrice}>Rp {myMoneyCart}</Text>
      </View>
    )
  },[myMoneyCart])

  const handleCreateTransaction = ()=>{
    cart?.data?.map((el,i)=>{
      console.log(el?.productId?._id)
      const data = {
          productId: el?.productId?._id,
          quantity: myMoneys?.counter
        }
      dispatch(CreateTransactionAction(data))
    })    
    props.navigation.navigate('Pembayaran',{ data : myMoneyCart })
  }

  const ModalDeletes = ()=>(
    <Modal
      isVisible={ModalDelete}
      onBackButtonPress={() => setModalDelete(false)}
      onBackdropPress={() => setModalDelete(false)}
      backdropTransitionOutTiming={100}
      style={{flex: 1, justifyContent: 'center'}}
    >
      <View style={{backgroundColor: 'white', paddingHorizontal: 5, paddingTop: 3, paddingBottom: 7, borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 15,borderBottomRightRadius: 15}}>
          <View style={{alignItems: 'center', justifyContent: 'center', marginVertical: 12}}>
            <Text style={{maxWidth: 200, textAlign: 'center'}}>Apakah Anda Yakin Ingin Menghapus Item ini ?</Text>
          </View>
          <View style={{marginBottom: 20, paddingHorizontal: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
              <TouchableOpacity onPress={()=> HanldeDestroyCartAction() } style={{backgroundColor: COLOR.green, width: 100, marginHorizontal: 10, borderRadius: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Ya</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> setModalDelete(false) } style={{backgroundColor: COLOR.green, width: 100, marginHorizontal: 10, borderRadius: 20}}>
                <Text style={{marginVertical: 15, color: 'white', textAlign: 'center'}}>Tidak</Text>
              </TouchableOpacity>
          </View>
      </View>
    </Modal>
  )

  const RenderMyProduct = ({item,index})=>{
    return(
      <View style={styles.image}>
        <View style={styles.images}>
          <Image 
            source={{ uri: `${BASE_URL}/${item?.productId?.image}` }} 
            style={{width: 100, height: 100}}/>
        </View>
        <View style={styles.containerDetail}>
            <Text style={styles.detailTitle}>{item?.productId?.name}</Text>
            <Text style={styles.detailPrice}>Rp {myMoneyCart}</Text>
            <View style={styles.containerCounter}>
              <TouchableOpacity onPress={()=>{
                dispatch(addItemToCart({
                  type: types.DECERMENT,
                  payload: counter - 1,
                  name : item?.productId?.name 
                }))
              }}>
              {/* <TouchableOpacity onPress={Decerment(item,index)}> */}
              <FontAwesome name='minus' color={COLOR.black} size={15} style={{marginLeft: 15}}/>
              </TouchableOpacity>
              <Text style={{ marginHorizontal: 5 }}>{counter}</Text>
              <TouchableOpacity onPress={()=>{
                dispatch(addItemToCart({
                  type: types.INCERMENT,
                  payload: counter + 1,
                  name : item?.productId?.name 
                }))
              }}>
              <FontAwesome 
                name='plus'
                color={COLOR.black}
                size={15}
                style={{marginRight: 15}}
              />
              </TouchableOpacity>
            </View>
        </View> 
        <TouchableOpacity 
          style={{marginLeft: 50, marginTop: 30, }}
          onPress={() => HanldeDestroyCart(item?._id) }
          >
          <FontAwesome 
            name='trash'
            color={COLOR.red}
            size={30}
          />
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      {ModalDeletes()}
      <View style={styles.header}>
          <Image 
            source={require('../../assets/Images/logoFix.png')}
            style={styles.logo}
          />
          <Text style={styles.title}>My Cart</Text>
          <View style={styles.icon}>
            <TouchableOpacity 
              style={{marginRight: 10, marginTop: 7}}
              >
              <FontAwesome
                name='shopping-basket'
                color={COLOR.green}
                size={23}
              />
            </TouchableOpacity>
            <Text style={styles.counter}>{counter}</Text>
            <TouchableOpacity 
              style={{marginLeft: 10, marginTop: 3}}
              onPress={() => navigation.navigate('Home')}
              >
              <FontAwesome 
                name='home'
                color={COLOR.green}
                size={33}
              />
            </TouchableOpacity>
          </View>
      </View>
        {
          loading ? (
            <>
            <SkeletonPlaceholder style={{ padingHorizontal: 5}}>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                <View style={{ width: 90, height: 90, borderRadius: 10 }} />
                <View style={{ marginLeft: 20 }}>
                  <View style={{ width: 120, height: 15, borderRadius: 4 }} />
                  <View
                    style={{ marginTop: 6, width: 200, height: 15, borderRadius: 4 }}
                  />
                </View>
              </View>
            </SkeletonPlaceholder>
            <SkeletonPlaceholder style={{ padingHorizontal: 5}}>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                <View style={{ width: 90, height: 90, borderRadius: 10 }} />
                <View style={{ marginLeft: 20 }}>
                  <View style={{ width: 120, height: 15, borderRadius: 4 }} />
                  <View
                    style={{ marginTop: 6, width: 200, height: 15, borderRadius: 4 }}
                  />
                </View>
              </View>
            </SkeletonPlaceholder>
            <SkeletonPlaceholder style={{ padingHorizontal: 5}}>
              <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                <View style={{ width: 90, height: 90, borderRadius: 10 }} />
                <View style={{ marginLeft: 20 }}>
                  <View style={{ width: 120, height: 15, borderRadius: 4 }} />
                  <View
                    style={{ marginTop: 6, width: 200, height: 15, borderRadius: 4 }}
                  />
                </View>
              </View>
            </SkeletonPlaceholder>
            </>
          ) : (
            <FlatList 
              data={cart?.data}
              keyExtractor={(item,index)=> index.toString()}
              renderItem={({item,index}) => <RenderMyProduct item={item} index={index}/>}
            />
          )
        }
        <View style={styles.containerSubTotal}>
          {SubTotal()}
          <View>
            <TouchableOpacity 
              onPress={() => handleCreateTransaction()}
              style={styles.containerCheckout}>
              <Text style ={styles.titleCheckout}>Checkout</Text>
            </TouchableOpacity>
          </View>
        </View>
      <View>
      </View>
    </ScrollView>
  );
};

export default Cart;

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: "#ffff"
  },
  logo: {
    height: 60, 
    width: 60, 
    marginLeft: 10
  },
  header: {
    backgroundColor: COLOR.white, 
    marginTop: 50, 
    height: 60, 
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  title: {
    fontSize: 20, 
    marginLeft: -130, 
    marginTop: 20
  },
  icon: {
    flexDirection: 'row', 
    marginRight: 15, 
    marginTop: 10
  },
  counter: {
    marginLeft: -15, 
    marginTop: -6, 
    fontSize: 20, 
    backgroundColor: COLOR.orange, 
    borderRadius: 30, 
    width: 25, 
    height: 25, 
    textAlign: 'center', 
    color:COLOR.black
  },
  image: {
    flexDirection: 'row', 
    backgroundColor:COLOR.white, 
    marginTop: 15, 
    height: 100, 
    width: 370, 
    marginLeft: 10, 
    borderRadius: 10
  },
  images: {
    backgroundColor: COLOR.yellow, 
    width: 100, 
    height: 100, 
    borderRadius: 10
  },
  containerDetail: {
    marginLeft: 15, 
    marginTop: 10
  },
  detailTitle: {
    fontSize: 16, 
    fontWeight: '700'
  },
  detailPrice: {
    color: COLOR.red, 
    marginTop: 5
  },
  containerCounter: {
    flexDirection: 'row', 
    borderWidth: 0.5, 
    justifyContent: 'space-between', 
    marginTop: 10, 
    borderRadius: 30, 
    height: 30, 
    alignItems: 'center', 
    borderColor: COLOR.darkGreen
  },
  containerSubTotal: {
    backgroundColor: COLOR.white, 
    marginTop: 460, 
    height: 100
  },
  containerTotal: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    marginTop: 10
  },
  titleTotal: {
    paddingLeft: 15, 
    fontSize: 20, 
    fontWeight: '700'
  },
  totalPrice: {
    paddingRight: 15, 
    fontSize: 22, 
    fontWeight: '700', 
    color: COLOR.red
  },
  containerCheckout: {
    backgroundColor: COLOR.green, 
    height: 50, 
    width: 360, 
    justifyContent: 'center', 
    alignSelf: 'center', 
    borderRadius: 10
  },
  titleCheckout: {
    alignSelf: 'center', 
    fontSize: 20, 
    fontWeight: 'bold', 
    color: COLOR.white
  }
})


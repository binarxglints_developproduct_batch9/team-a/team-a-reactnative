import React, {useEffect} from 'react'
import { 
  ScrollView,StyleSheet, 
  Text, View, 
  StatusBar, Image, FlatList
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

import {useSelector, useDispatch} from 'react-redux';
import { getProductsAction } from '../../../store/action/productAction';
import {userProfileAction} from '../../../store/action/userProfileAction'

import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import COLOR from '../../utils/Tools';
import Banner from '../../components/Banner';
import Category from '../../components/Banner/Category';
import ProductCard from '../../components/productCard';

const Home = ({navigation}) => {
  const listProduct= useSelector( state => state.productReducer )

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProductsAction());
    dispatch(userProfileAction())
  }, []);
  console.log('====== listProduct', listProduct.product)

  const numColumns = 2;

  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={styles.header}>
        <Image 
          source={require('../../assets/Images/logoFix.png')}
          style={styles.logo}
        />
        <TouchableOpacity 
          onPress={()=>navigation.navigate('Search')}
          style={styles.searchBox}
        >
          <Text style={{marginLeft: 10, color:COLOR.black, opacity: 0.5}}>Search vegetables, fruits...</Text>
          <Ionicons 
            name='search-outline'
            color={COLOR.green}
            size={25}
            style={{ alignItems:'center', marginLeft: 10 }}
          />
        </TouchableOpacity>
      <View style={styles.headerIcon}>
        <TouchableOpacity style={{marginLeft: 10}}>
          <FontAwesome 
            name='bell'
            color={COLOR.green}
            size={25}
            onPress={() => navigation.navigate('Notification')}
          />
        </TouchableOpacity>
        <TouchableOpacity 
          style={{marginLeft: 10}}
          onPress={() => navigation.navigate('Cart')}
          >
          <FontAwesome 
            name='shopping-basket'
            color={COLOR.green}
            size={25}
          />
        </TouchableOpacity>
      </View>
      </View>

      <TouchableOpacity>
        <Banner />
      </TouchableOpacity>

      <Category />

      <View>
      <FlatList showsHorizontalScrollIndicator={false}
            data={listProduct.product}
            numColumns={numColumns}
            renderItem={({ item }) => {
              return (
                <ProductCard
                  id={item.id}
                  name={item.name}
                  image={item.image}
                  price={item.price}
                  detailProduct={() =>
                    navigation.navigate(
                      "Detail Product",
                      {
                        id: item.id,
                        name: item.name,
                        image: item.image,
                        price: item.price,
                        description: item.desc,
                      })}
                />
              )
            }}
            keyExtractor={item => item.id}
          />
          
      </View>
    </ScrollView>
  )
}

export default Home

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
  header: {
    flexDirection: 'row', 
    backgroundColor: COLOR.pureWhite,
    marginTop: 50
  },
  logo: {
    height: 60, 
    width: 60, 
    marginLeft: 10
  },
  searchBox: {
    borderWidth:1,
    width: 220, 
    height: 40, 
    marginTop:10, 
    marginLeft: 7, 
    borderRadius: 10,
    borderColor: COLOR.green,
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerIcon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  
})

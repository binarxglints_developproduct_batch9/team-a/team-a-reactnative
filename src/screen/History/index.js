import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ScrollView, StatusBar, Image, FlatList, } from 'react-native'
import COLOR from '../../utils/Tools/index';
import { useSelector, useDispatch } from 'react-redux';
import HistoryCard from '../../components/History Card';
import { historyBuyerAction } from '../../../store/action/historyBuyerAction';

const History = () => {

  const data = useSelector(state => state.historyBuyerReducer.data)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(historyBuyerAction())
  }, [])
  console.log('ini data buyer', data)

  return (
    
      <ScrollView style={styles.container}>
        <StatusBar 
          backgroundColor={'transparent'} 
          barStyle='dark-content'
          translucent={true}
        />
        <View style={styles.header}>
          <Image 
            source={require('../../assets/Images/logoFix.png')}
            style={styles.logo}
          />
          <Text style={styles.title}>History Transaction</Text>
        </View>
        <FlatList 
          data={data}
          renderItem={({item}) => {
            console.log('yopi dablek',item)
            return (
              <HistoryCard 
                id={item.productId[0]._id}
                name={item.productId[0].name}
                image={item.productId[0].image}
                price={item.productId[0].price.$numberDecimal}
                status={item.status}
                sellerImage={item.productId[0].seller.image}
                sellerUsername={item.productId[0].seller.username}
              />
            )
          }}
          keyExtractor={item => item.id}
        />
        
    </ScrollView>
  )
}

export default History

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
  header: {
    flexDirection: 'row', 
    backgroundColor: COLOR.pureWhite,
    marginTop: 50
  },
  logo: {
    height: 60, 
    width: 60, 
    marginLeft: 10
  },
  title: {
    fontSize: 20,
    marginTop: 15,
    marginLeft: 15
  }
})

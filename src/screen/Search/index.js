import React, { useEffect, useState } from 'react'
import { 
  StyleSheet, Text, 
  View, ScrollView, 
  StatusBar, TouchableOpacity,
  Image, TextInput, Button, FlatList, ClippingRectangle
} from 'react-native'

import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import COLOR from '../../utils/Tools';
import axios from 'axios';
import {BASE_URL} from '../../../store/constant/general';
import ProductCard from '../../components/productCard';

const Search = ({navigation}) => {

  const [ search, setSearch ] = useState('');
  const [ searchName, setSearchName ] = useState([]);
  const [ searchCategory, setSearchCategory ] = useState([]);
  const [ category, setCategory ] = useState('');

  const handleSearchCategory = async() => {
    await axios({
      method: 'GET',
      url: `${BASE_URL}/home/category/${category}`
    })
    .then(response => setSearchCategory(response.data.data))
    .catch(err => console.log(err.message))
  }

  const handleSearch = async(search) => {
    await axios({
      method: 'GET',
      url: `${BASE_URL}/home/find/${search}`
    })
    .then(response => setSearchName(response.data.data))
    .catch(err => console.log(err.message))
  }

  console.log('category', category)
  console.log(searchCategory)

  const numColumns = 2;
    
  useEffect(()=>{
		handleSearchCategory()
  },[category])
  
  useEffect (() => {
    handleSearch(search)
  }, [search, searchCategory])

  console.log(search)

  return (
    <ScrollView style={styles.container}>
      <StatusBar 
        backgroundColor={'transparent'} 
        barStyle='dark-content'
        translucent={true}
      />
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome 
            name="arrow-left"
            size={25}
            style={styles.buttonBack}
            onPress={() => navigation.navigate ('Home')}
          />
        </TouchableOpacity>
        <TextInput
          placeholder="Search vegetables, fruits..."
          style={styles.searchBox}
          value={search}
          onChangeText={(text) => setSearch(text)}
          onSubmitEditing={({ nativeEvent }) => {
            setSearchCategory([])
            setSearch(nativeEvent.text)}}
        />
        <TouchableOpacity style={{}}>
          <Ionicons 
            name='search-outline'
            color={COLOR.green}
            size={27}
            style={styles.searchIcon}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.titleBox}>
        <Text style={styles.title}>SEARCH BY CATEGORY</Text>
        <View style={styles.line}></View>
        <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 30}}>
        <TouchableOpacity 
        onPress={()=> { setCategory('vegetable')
         setSearchName([])
         setSearch('')
         handleSearchCategory()
        }}
        style={styles.buttonCategory}>
          <Text style={styles.textButton}>Vegetables</Text>
        </TouchableOpacity>
          <TouchableOpacity 
            onPress={()=> { setCategory('fruit')
            setSearchName([])
            setSearch('')
            handleSearchCategory()
            }}
            style={styles.buttonCategory}>
            <Text style={styles.textButton}>Fruits</Text>
          </TouchableOpacity>
        </View>
      </View>
      { searchCategory !== [] && <FlatList 
        data={searchCategory}
        numColumns={numColumns}
        renderItem={({ item }) => {
          return (
            <ProductCard 
              id={item.id}
              name={item.name}
              image={item.image}
              price={item.price}
              detailProduct={() =>
                navigation.navigate(
                  "Detail Product",
                  {
                    id: item.id,
                    name: item.name,
                    image: item.image,
                    price: item.price,
                    description: item.desc,
                  })}
            />
          )
        }}
      />}
       {search != '' &&  <FlatList 
        data={searchName}
        numColumns={numColumns}
        renderItem={({ item }) => {
          return (
            <ProductCard 
              id={item.id}
              name={item.name}
              image={item.image}
              price={item.price}
              detailProduct={() =>
                navigation.navigate(
                  "Detail Product",
                  {
                    id: item.id,
                    name: item.name,
                    image: item.image,
                    price: item.price,
                    description: item.desc,
                  })}
            />
          )
        }}
      />}
      {/* <View style={{justifyContent: 'center', alignItems: 'center', margin: 100}}>
        <Button 
          title="Detail Product"
          onPress={()=> navigation.navigate('Detail Product')}
        />
      </View> */}
    </ScrollView>
  )
}

export default Search

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: (COLOR.grey)
  },
  header: {
    flexDirection: 'row', 
    backgroundColor: COLOR.pureWhite,
    marginTop: 50,
    height: 60
  },
  buttonBack: { 
    marginLeft: 10,
    alignSelf: 'center',
    width: 50,
    marginTop: 17,
    color: COLOR.green
  },
  searchBox: {
    borderWidth:2,
    width: 320, 
    height: 40, 
    marginTop:10, 
    marginLeft: -5,
    borderRadius: 10,
    borderColor: COLOR.green,
    flexDirection: 'row',
    alignItems: 'center'
  },
  searchIcon: { 
    alignItems:'center', 
    marginLeft: -30, 
    marginTop: 15 
  },
  titleBox: {
    backgroundColor: COLOR.pureWhite, 
    height: 150, 
    justifyContent: 'flex-start', 
    marginTop: 5
  },
  title :{
    fontWeight: '700', 
    marginLeft: 15, 
    marginTop: 10, 
    paddingVertical: 10 
  },
  line: {
    borderBottomWidth: 1, 
    opacity: 0.5
  },
  buttonCategory: {
    borderWidth: 2, 
    width: 80, 
    borderRadius: 5, 
    borderColor: COLOR.green,
    backgroundColor: COLOR.green,
    marginLeft: 15
  },
  textButton: {
    color: COLOR.pureWhite, 
    alignSelf: 'center'
  }
})

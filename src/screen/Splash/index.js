import React, {useEffect} from 'react'
import { 
  StyleSheet, Text, 
  View, StatusBar, 
  Image 
} from 'react-native'

import COLOR from '../../utils/Tools';

const Splash = ({navigation}) => {

  useEffect(() => {
      setTimeout( () => {
        navigation.replace('Login');
      }, 5000)
    }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar 
        backgroundColor={(COLOR.pureWhite)} 
        barStyle='dark-content'
      />
        <Image 
          source={require('../../assets/Images/logoFix.png')}
          style={styles.logo}
        />
    </View>
  )
}

export default Splash;

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: COLOR.pureWhite
  },
  logo: {
    width: 300,
    height: 300
  }
})

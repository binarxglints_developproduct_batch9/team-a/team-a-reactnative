import icUp from './up.png'
import icDown from './down.png'
import DuBni from './bni.png'
import icCamera from './camera.png'
import icGallery from './gallery.png'

export {
    icUp,
    icDown,
    DuBni,
    icCamera,
    icGallery,
}
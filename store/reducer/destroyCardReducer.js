import * as Types from '../constant/actionTypes'

const initiallState = {
    loading: false,
    destroyed: null,
    error: false,
}

const destroyCartReducer = (state = initiallState, action)=>{
    switch(action.type){
        case Types.DESTROY_CART_REQUEST:
            return {
                loading: true,
                destroyed: null,
                error: false,
            }
        case Types.DESTROY_CART_SUCCESS:
            return {
                loading: false,
                destroyed: action.payload,
                error: false,
            }
        case Types.DESTROY_CART_FAILURE:
            return {
                loading: false,
                destroyed: null,
                error: action.error,
            }
        default:
            return { ...state }
    }
}

export default destroyCartReducer;
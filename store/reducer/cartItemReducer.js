import * as types from '../constant/actionTypes';

const initialState = {
  counter: 1,
}

const cartItemsReducer = ( state = initialState, action ) => {
  const { type, payload  } = action
  switch(action.type){
    case types.INCERMENT:
      return {
        ...state,
        counter: payload,
      }
    case types.DECERMENT:
      return {
        ...state,
        counter: payload,
      }
    default:
      return { ...state }
  }
}

export default cartItemsReducer;
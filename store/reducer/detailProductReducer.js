import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  detail: {},
  error: null,     
};

function detailProductReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_DETAIL_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.GET_DETAIL_SUCCESS:
      return Object.assign({},state, {
        loading: false,
        detail: {...action.payload},
        
      });
    case types.GET_DETAIL_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      });
    default:
      return state;
  }
}

export default detailProductReducer;



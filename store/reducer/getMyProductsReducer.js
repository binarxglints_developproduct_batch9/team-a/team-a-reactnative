import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  data: {},
  error: null
};

function MyProductsReducer (state= initialState, action) {
  switch ( action.type ){
    case types.GET_MYPRODUCTS_REQUEST:
      return Object.assign({}, state, {
        loading: true
      });
    case types. GET_MYPRODUCTS_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        data: action.payload
      });
    case types.GET_MYPRODUCTS_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })
    default:
      return state;
  }
}

export default MyProductsReducer;
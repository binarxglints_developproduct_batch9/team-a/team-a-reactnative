import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  data: {},
  error: null
};

function NotificationSellerReducer (state= initialState, action) {
  switch ( action.type ){
    case types.NOTIFICATION_SELLER_REQUEST:
      return Object.assign({}, state, {
        loading: true
      });
    case types.NOTIFICATION_SELLER_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        data: action.payload
      });
    case types.NOTIFICATION_SELLER_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })
    default:
      return state;
  }
}

export default NotificationSellerReducer;
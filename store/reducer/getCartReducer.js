import * as types from '../constant/actionTypes'

const initialState = {
    loading: false,
    cart: [],
    error: null
}

const getCartReducer = (state = initialState, action) => {
    switch(action.type){
        case types.GET_CART_REQUEST:
            return {
                loading: true,
                cart: null,
                error: null
            }
        case types.GET_CART_SUCCESS:
            return{
                loading: false,
                cart: action.payload,
                error: null
            }
        case types.GET_CART_FAILURE:
            return{
                loading: false,
                cart: null,
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default getCartReducer;
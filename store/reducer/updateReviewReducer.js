import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  review: [],
  error: null,     
};

function updateReviewReducer(state = initialState, action) {
  switch (action.type) {
    case types.UPDATE_REVIEW_REQUEST:
      return {
        loading: true,
        review: null,
        error: null
      }
    case types.UPDATE_REVIEW_SUCCESS:
      return {
        loading: false,
        review: action.payload,  
        error: null,
      }
    case types.UPDATE_REVIEW_FAILURE:
      return {
        loading: false,
        error: action.error
      }
    default:
      return { ...state }
  }
}

export default updateReviewReducer;
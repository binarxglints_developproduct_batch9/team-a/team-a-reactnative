import * as Types from '../constant/actionTypes'


const initialState = {
    loading: false,
    data: null,
    error: false,
}

function createTransactionReducer(state = initialState, action){
    switch(action.type){
        case Types.CREATE_TRANSACTION_REQUEST:
         return  {
             loading: true,
             data: null,
             error: null
         } 
         case Types.CREATE_TRANSACTION_SUCCESS:
             return {
                 loading: false,
                 data: action.payload,
                 error: null
             }
        case Types.CREATE_TRANSACTION_FAILURE:
            return {
                loading: false,
                data: null,
                error: action.error
            }
        default:
            return { ...state }
    }
}

export default createTransactionReducer
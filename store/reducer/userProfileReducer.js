import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  user: [],
  error: null
};

function userProfileReducer (state= initialState, action) {
  switch ( action.type ){
    case types.GET_USER_PROFILE_REQUEST:
      return Object.assign({}, state, {
        loading: true
      });
    case types.GET_USER_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        user: action.payload
      });
    case types.GET_USER_PROFILE_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })
    default:
      return state;
  }
}

export default userProfileReducer;
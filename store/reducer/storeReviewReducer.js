import * as types from '../constant/actionTypes'


const initialState = {
    loading: false,
    reviews: null,
    error: false,
}


function storeReviewsReducer(state = initialState, action){
    switch(action.type){
        case types.STORE_REVIEW_REQUEST:
            return Object.assign({}, state,{
                loading: true,
            })
        case types.STORE_REVIEW_SUCCESS:
            return Object.assign({},state,{
                loading: false,
                reviews: action.payload,
            })
        case types.STORE_REVIEW_FAILURE:
            return Object.assign({}, state,{
                loading: false,
                error: action.error
            })
        default:
        return state;
    }
}

export default storeReviewsReducer;
import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  product: [],
  error: null,     
};

function productReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_PRODUCTS_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.GET_PRODUCTS_SUCCESS:
      return Object.assign({},state, {
        loading: false,
        product: action.payload,
        
      });
    case types.GET_PRODUCTS_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      });
    default:
      return state;
  }
}

export default productReducer;
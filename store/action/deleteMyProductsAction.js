import * as types from '../constant/actionTypes'
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';

export const deleteMyProductsRequest = () => ({
  type: types.DELETE_MYPRODUCTS_REQUEST,
});

export const deleteMyProductsSuccess = (item) => ({
  type: types.DELETE_MYPRODUCTS_SUCCESS,
  payload: item,
});

export const deleteMyProductsFailure = (error) => ({
  type: types.DELETE_MYPRODUCTS_FAILURE,
  error,
});

export const deleteMyProductsAction = (id) => {
  console.log(id)
  return async (dispatch) => {
    try {
      dispatch(deleteMyProductsRequest());
      const res = await axios.delete(`${BASE_URL}/home/delete/${id}`,{
          headers: {'Authorization': `bearer ${TOKEN}`}
      })
      dispatch(deleteMyProductsSuccess(res.data));
    } catch (error) {
      dispatch(deleteMyProductsFailure(error));
      console.log("Error Delete My Products",error)
    }
  }
}
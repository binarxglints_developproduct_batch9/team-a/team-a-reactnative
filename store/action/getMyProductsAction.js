import * as types from '../constant/actionTypes';
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';

export const myProductsRequest = () => ({
  type: types.GET_MYPRODUCTS_REQUEST
});

export const myProductsSuccess = (payload) => ({
  type: types.GET_MYPRODUCTS_SUCCESS,
  payload
});

export const myProductsFailure = (error) => ({
  type: types.GET_MYPRODUCTS_FAILURE,
  error
});

export const myProductsAction = ( payload ) => {
  console.log("payload product", payload)
  return async (dispatch) => {
    try {
      dispatch( myProductsRequest() );
      const res = await axios.get( `${BASE_URL}/home/seller/${payload}`, {
        headers: { Authorization: `Bearer ${TOKEN}`},
      });
      console.log('My Products:', res.data.data)
      dispatch( myProductsSuccess(res.data.data) )
    } catch (error) {
      console.log('Error My Products:', error);
      dispatch(myProductsFailure(error));
    }
  };
};
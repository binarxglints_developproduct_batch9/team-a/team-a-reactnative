import * as types from '../constant/actionTypes'
import axios from 'axios'
import { TOKEN, BASE_URL } from '../constant/general'
import { getCartAction } from './getCartAction'

export const addTocartRequest = ()=> ({
    type: types.ADD_TO_CART_REQUEST
})

export const addTocartSuccess = (detail)=> ({
    type: types.ADD_TO_CART_SUCCESS,
    payload: detail,
})

export const addTocartFailure = (error)=> ({
    type: types.ADD_TO_CART_FAILURE,
    error,
})


export const addTocartAction = (data)=>{
    return async (dispatch)=>{
        try {
            dispatch(addTocartRequest())
            const res = await axios.post(`${BASE_URL}/cart/create`,data,{
                headers: {'Authorization': `bearer ${TOKEN}`}
            })
            dispatch(addTocartSuccess(res?.data))
            dispatch(getCartAction())
            console.log("Add To Cart",res?.data)
        } catch (error) {
            dispatch(addTocartFailure(error))
            console.log("Add To Cart Failure",error)
        }
    }
}

import * as types from '../constant/actionTypes';
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';

export const userProfileRequest = () => ({
  type: types.GET_USER_PROFILE_REQUEST
});

export const userProfileSuccess = (payload) => ({
  type: types.GET_USER_PROFILE_SUCCESS,
  payload
});

export const userProfileFailure = (error) => ({
  type: types.GET_USER_PROFILE_FAILURE,
  error
});

export const userProfileAction = ( payload ) => {
  return async (dispatch) => {
    try {
      dispatch( userProfileRequest() );
      const res = await axios.get( `${BASE_URL}/profile`, {
        headers: { Authorization: `Bearer ${TOKEN}`},
      });
      dispatch( userProfileSuccess(res.data.data) )
    } catch (error) {
      console.log('Error UserProfile', error);
      dispatch(userProfileFailure(error));
    }
  };
};
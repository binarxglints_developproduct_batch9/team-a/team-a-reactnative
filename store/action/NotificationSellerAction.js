import * as types from '../constant/actionTypes';
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';

export const notificationSellerRequest = () => ({
  type: types.NOTIFICATION_SELLER_REQUEST
});

export const notificationSellerSuccess = (payload) => ({
  type: types.NOTIFICATION_SELLER_SUCCESS,
  payload
});

export const notificationSellerFailure = (error) => ({
  type: types.NOTIFICATION_SELLER_FAILURE,
  error
});

export const NotificationSellerAction = ( payload ) => {
  return async (dispatch) => {
    try {
      dispatch( notificationSellerRequest() );
      const res = await axios.get( `${BASE_URL}/transaction/seller`, {
        headers: { Authorization: `Bearer ${TOKEN}`},
      });
      console.log('Notif Sukses:', res.data.data)
      dispatch( notificationSellerSuccess(res.data.data) )
    } catch (error) {
      console.log('Notif Error:', error);
      dispatch(notificationSellerFailure(error));
    }
  };
};
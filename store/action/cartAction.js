import * as types from '../constant/actionTypes'

export const incerementAction = (detail)=>{
  return {
    type: types.INCERMENT,
    payload: detail,
  }
}

export const decermentAction = (detail)=>{
  return {
    type: types.DECERMENT,
    payload: detail,
  }
}

export const addItemToCart = (items) => {
  console.log(items)
  return async (dispatch)=>{
    switch(items.type){
      case 'INCERMENT':
        dispatch(incerementAction(items?.payload))
        case 'DECERMENT':
        dispatch(decermentAction(
          items?.payload < 1 ? items?.payload+ 1 : items?.payload
        ))
        
    }
  }
}
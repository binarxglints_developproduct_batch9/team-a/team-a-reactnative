import Axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'

export const getDiscusionRequest = ()=> ({
    type: types.GET_DISCUSION_REQUEST,
})
export const getDiscusionSuccess = (detail)=> ({
    type: types.GET_DISCUSION_SUCCESS,
    payload: detail
})
export const getDiscusionFailure = (error)=> ({
    type: types.GET_DISCUSION_FAILURE,
    error: error,
})

export const getDiscusionAction = (param)=>{
    return async (dispatch) => {
        dispatch(getDiscusionRequest())
        Axios.get(`${BASE_URL}/discuss/${param}`,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(getDiscusionSuccess(res?.data))
            console.log("Success Discusion",res?.data)
        })
        .catch((error)=>{
            dispatch(getDiscusionFailure(error))
            console.log("Gagagl Discusion",error)
        })
    }
}
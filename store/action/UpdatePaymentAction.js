import * as types from '../constant/actionTypes'

import axios from 'axios'
import { BASE_URL, TOKEN } from '../constant/general'

export const updatePaymentRequest = ()=> ({
    type: types.UPDATE_PAYMENT_REQUEST,
})

export const updatePaymentSuccess = (detail)=>({
    type: types.UPDATE_PAYMENT_SUCCESS,
    payload: detail,
})

export const updatePaymentFailure = (error)=>({
    type: types.UPDATE_REVIEW_FAILURE,
    error,
})

export const updateConfirmationPaymentAction = (data)=>{
    console.log(data)
    return async (dispatch)=>{
        dispatch(updatePaymentRequest());
        const photoForUploaded  = new FormData()
        photoForUploaded.append('payment', data?.data)
        axios.put(`${BASE_URL}/transaction/update/payment/6015108fed17ac444ff7d46b`,photoForUploaded,{
            headers: {
                'Authorization': `bearer ${TOKEN}`,
                'Content-Type': 'multipart/form-data; ',
            },
        })
        .then((res) => {
            dispatch(updatePaymentSuccess(res?.data))
            console.log("success verivy transaction",res?.data)
        })
        .catch((error)=>{
            dispatch(updatePaymentFailure(error))
            console.log("Ggagal verivy transaction",error)
        })
    }
}
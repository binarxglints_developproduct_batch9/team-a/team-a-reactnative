import * as Types from '../constant/actionTypes'
import { BASE_URL,TOKEN } from '../constant/actionTypes'
import axios from 'axios'
import { getCartAction } from './getCartAction'

export const createTransactionRequest = ()=>({
    type: Types.CREATE_TRANSACTION_REQUEST,
})

export const createTransactionSuccess = (detail)=>({
    type: Types.CREATE_TRANSACTION_SUCCESS,
    payload: detail
})

export const createTransactionFailure = (error)=>({
    type: Types.CREATE_TRANSACTION_FAILURE,
    error: error
})


export const CreateTransactionAction = (data)=>{
    console.log(data)
    return async (dispatch) =>{
        dispatch(createTransactionRequest())
        // axios.post(`${BASE_URL}/transaction/create`,data,{
        //     headers: {
        //         'Authorization': `bearer ${TOKEN}`,
        //         'Access-Control-Allow-Origin': '*',
        //         'Access-Control-Allow-Headers': '*',
        //     }
        // })
        // fetch(`${BASE_URL}/transaction/create`, {
        //     method: 'POST',
        //     headers: {
        //       Accept: 'application/json',
        //       'Content-Type': 'application/json',
        //       'Authorization': `bearer ${TOKEN}`
        //     },
        //     body: data,
        // })
        .then((res)=>{
            dispatch(createTransactionSuccess(res?.data))
            dispatch(getCartAction())
            console.log("Succes ?",res?.data)
        })
        .catch((error)=>{
            dispatch(createTransactionFailure(error))
            console.log("Gagal ?",error)
        })
    }
}
import * as types from '../constant/actionTypes'
import axios from 'axios'
import { BASE_URL, TOKEN } from '../constant/general'

export const getReviewRequest = ()=> ({
    type: types.GET_REVIEW_REQUEST,
})

export const getReviewSuccess = (detail)=>({
    type: types.GET_REVIEW_SUCCESS,
    payload: detail,
})
export const getReviewFailure = (error)=>({
    type: types.GET_REVIEW_FAILURE,
    error,
})

export const getReviewAction = (id)=>{
    return async (dispatch)=>{
        dispatch(getReviewRequest());
        axios.get(`${BASE_URL}/review/${id}`,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res) => {
            dispatch(getReviewSuccess(res?.data?.data))
        })
        .catch((error)=>{
            dispatch(getReviewFailure(error))
        })
    }
}
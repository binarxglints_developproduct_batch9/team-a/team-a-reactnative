import * as Types from '../constant/actionTypes'
import axios from 'axios'
import { BASE_URL, TOKEN } from '../constant/general'
import { getCartAction } from './getCartAction'

export const destroyCartRequest = ()=> ({
    type : Types.DESTROY_CART_REQUEST
})

export const destroyCartSuccee = (detail)=> ({
    type : Types.DESTROY_CART_SUCCESS,
    payload: detail,
})

export const destroyCartFailure = (error)=> ({
    type : Types.DESTROY_PRODUCTS_FAILURE,
    error: error,
})


export const destroyCartAction = (id)=>{
    return async (dispatch) =>{
        dispatch(destroyCartRequest())
        axios.delete(`${BASE_URL}/cart/delete/${id}`,{
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(destroyCartSuccee(res?.data))
            dispatch(getCartAction())
            console.log("Succes ?",res?.data)
        })
        .catch((error)=>{
            dispatch(destroyCartFailure(error))
            console.log("Gagal ?",error)
        })
    }
}

import Axios from 'axios'
import * as types from '../constant/actionTypes'
import { BASE_URL, TOKEN } from '../constant/general'
import { getDiscusionAction } from './getDiscusionAction'

export const postDiscusionRequest = ()=> ({
    type: types.POST_DISCUSION_REQUEST,
})
export const postDiscusionSuccess = (detail)=> ({
    type: types.POST_DISCUSION_SUCCESS,
    payload: detail
})
export const postDiscusionFailure = (error)=> ({
    type: types.POST_DISCUSION_FAILURE,
    error: error,
})

export const postDiscusionAction = (data)=>{
    console.log("===DATA===",data)
    return async (dispatch) => {
        dispatch(postDiscusionRequest())
        Axios.post(`${BASE_URL}/discuss/${data?.param}/create`,
        data?.data,
        {
            headers: {'Authorization': `bearer ${TOKEN}`}
        })
        .then((res)=>{
            dispatch(postDiscusionSuccess(res?.data))
            dispatch(getDiscusionAction(data?.param))
            console.log("Success Discusion",res?.data)
        })
        .catch((error)=>{
            dispatch(postDiscusionFailure(error))
            console.log("Gagagl Discusion",error)
        })
    }
}
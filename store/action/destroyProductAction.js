import * as types from '../constant/actionTypes'
import axios from 'axios';
import { BASE_URL, TOKEN } from '../constant/general';

export const destroyProductsRequest = () => ({
  type: types.DESTROY_PRODUCTS_REQUEST,
});

export const destroyProductsSuccess = (detail) => ({
  type: types.DESTROY_PRODUCTS_SUCCESS,
  payload: detail,
});

export const destroyProductsFailure = (error) => ({
  type: types.DESTROY_PRODUCTS_FAILURE,
  error,
});

export const destroyProductsAction = (id) => {
  return async (dispatch) => {
    try {
      dispatch(destroyProductsRequest());
      const res = await axios.delete(`${BASE_URL}/home/delete/${id}`,{
          headers: {'Authorization': `bearer ${TOKEN}`}
      })
      dispatch(destroyProductsSuccess(res.data.data));
    } catch (error) {
      dispatch(destroyProductsFailure(error));
      console.log("Horeeee success",error)
    }
  }
}